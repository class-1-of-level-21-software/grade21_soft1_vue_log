Vue 计算属性和绑定HTML-class
===

一 .基础示例​
---
+ 模板中的表达式虽然方便，但也只能用来做简单的操作。如果在模板中写太多逻辑，会让模板变得臃肿，难以维护
    > 比如说，我们有这样一个包含嵌套数组的对象：
    ```js
    export default {
    data(){
        return {
            author: {
                name: 'John Doe',
                books: [
                'Vue 2 - Advanced Guide',
                'Vue 3 - Basic Guide',
                'Vue 4 - The Mystery'
                ]
                }
            }
        }
    }
    ```
    > 我们想根据 author 是否已有一些书籍来展示不同的信息：
    ```js
    <p>Has published books:</p>
    <span>{{ author.books.length > 0 ? 'Yes' : 'No' }}</span>
    ```
    + 这里的模板看起来有些复杂。我们必须认真看好一会儿才能明白它的计算依赖于 author.books。更重要的是，如果在模板中需要不止一次这样的计算，我们可不想将这样的代码在模板里重复好多遍

    > 因此我们推荐使用计算属性来描述依赖响应式状态的复杂逻辑。这是重构后的示例：

    ```js
    export default {
    data() {
        return {
        author: {
            name: 'John Doe',
            books: [
            'Vue 2 - Advanced Guide',
            'Vue 3 - Basic Guide',
            'Vue 4 - The Mystery'
            ]
        }
        }
    },
    computed: {
        // 一个计算属性的 getter
        publishedBooksMessage() {
        // `this` 指向当前组件实例
        return this.author.books.length > 0 ? 'Yes' : 'No'
        }
    }
    }
    ```
    ```js
    <p>Has published books:</p>
    <span>{{ publishedBooksMessage }}</span>
    ```

    > 我们在这里定义了一个计算属性 publishedBooksMessage

    + 更改此应用的 data 中 books 数组的值后，可以看到 publishedBooksMessage 也会随之改变

+ 在模板中使用计算属性的方式和一般的属性并无二致。Vue 会检测到 this.publishedBooksMessage 依赖于 this.author.books，所以当 this.author.books 改变时，任何依赖于 this.publishedBooksMessage 的绑定都将同时更新

二 .计算属性缓存 vs 方法​
---
+ 你可能注意到我们在表达式中像这样调用一个函数也会获得和计算属性相同的结果：
    ```js
    <p>{{ calculateBooksMessage() }}</p>
    ```
    ```js
    // 组件中
    methods: {
    calculateBooksMessage() {
        return this.author.books.length > 0 ? 'Yes' : 'No'
    }
    }
    ```
    > 若我们将同样的函数定义为一个方法而不是计算属性，两种方式在结果上确实是完全相同的，然而，不同之处在于计算属性值会基于其响应式依赖被缓存。一个计算属性仅会在其响应式依赖更新时才重新计算。这意味着只要 author.books 不改变，无论多少次访问 publishedBooksMessage 都会立即返回先前的计算结果，而不用重复执行 getter 函数

    + 这也解释了为什么下面的计算属性永远不会更新，因为 Date.now() 并不是一个响应式依赖

    ```js
    computed: {
    now() {
        return Date.now()
    }
    }
    ```
    > 相比之下，方法调用总是会在重渲染发生时再次执行函数。

    + 为什么需要缓存呢？想象一下我们有一个非常耗性能的计算属性 list，需要循环一个巨大的数组并做许多计算逻辑，并且可能也有其他计算属性依赖于 list。没有缓存的话，我们会重复执行非常多次 list 的 getter，然而这实际上没有必要！如果你确定不需要缓存，那么也可以使用方法调用

三 .可写计算属性​
---
+ 计算属性默认是只读的。当你尝试修改一个计算属性时，你会收到一个运行时警告。只在某些特殊场景中你可能才需要用到“可写”的属性，你可以通过同时提供 getter 和 setter 来创建：
    ```js
    export default {
    data() {
        return {
        firstName: 'John',
        lastName: 'Doe'
        }
    },
    computed: {
        fullName: {
        // getter
        get() {
            return this.firstName + ' ' + this.lastName
        },
        // setter
        set(newValue) {
            // 注意：我们这里使用的是解构赋值语法
            [this.firstName, this.lastName] = newValue.split(' ')
        }
        }
    }
    }
    ```
    > 现在当你再运行 this.fullName = 'John Doe' 时，setter 会被调用而 this.firstName 和 this.lastName 会随之更新

四 .最佳实践​
---
### Getter 不应有副作用​
> 计算属性的 getter 应只做计算而没有任何其他的副作用，这一点非常重要，请务必牢记。举例来说，不要在 getter 中做异步请求或者更改 DOM！一个计算属性的声明中描述的是如何根据其他值派生一个值。因此 getter 的职责应该仅为计算和返回该值。在之后的指引中我们会讨论如何使用监听器根据其他响应式状态的变更来创建副作用

### 避免直接修改计算属性值​
> 从计算属性返回的值是派生状态。可以把它看作是一个“临时快照”，每当源状态发生变化时，就会创建一个新的快照。更改快照是没有意义的，因此计算属性的返回值应该被视为只读的，并且永远不应该被更改——应该更新它所依赖的源状态以触发新的计算

五 .绑定 HTML class​
---
### 1. 绑定对象​
+ 我们可以给 :class (v-bind:class 的缩写) 传递一个对象来动态切换 class：
    ```js
    <div :class="{ active: isActive }"></div>
    ```
    > 上面的语法表示 active 是否存在取决于数据属性 isActive 的真假值。

    > 你可以在对象中写多个字段来操作多个 class。此外，:class 指令也可以和一般的 class attribute 共存。举例来说，下面这样的状态：

    ```js
    data() {
        return {
        isActive: true,
        hasError: false
        }
    }```
    > 配合以下模板：

    ```js
    <div
    class="static"
    :class="{ active: isActive, 'text-danger': hasError }"
    ></div>
    ```
    > 渲染的结果会是：

    ```js
    <div class="static active"></div>
    ```
    > 当 isActive 或者 hasError 改变时，class 列表会随之更新。举例来说，如果 hasError 变为 true，class 列表也会变成 "static active text-danger"

+ 绑定的对象并不一定需要写成内联字面量的形式，也可以直接绑定一个对象：
    ```js
    data() {
    return {
        classObject: {
        active: true,
        'text-danger': false
        }
    }
    }
    ```
    ```js
    <div :class="classObject"></div>
    ```
    > 这也会渲染出相同的结果。我们也可以绑定一个返回对象的计算属性。这是一个常见且很有用的技巧：

    ```js
    data() {
    return {
        isActive: true,
        error: null
    }
    },
    computed: {
    classObject() {
        return {
        active: this.isActive && !this.error,
        'text-danger': this.error && this.error.type === 'fatal'
        }
    }
    }```
    ```js
    <div :class="classObject"></div>
    ```
### 2. 绑定数组​
+ 我们可以给 :class 绑定一个数组来渲染多个 CSS class：
    ```js
    data() {
        return {
            activeClass: 'active',
            errorClass: 'text-danger'
        }
    }
    ```
    ```js
    <div :class="[activeClass, errorClass]"></div>
    ```
    > 渲染的结果是：

    ```js
    <div class="active text-danger"></div>
    ```
+ 如果你也想在数组中有条件地渲染某个 class，你可以使用三元表达式：

    ```js
    <div :class="[isActive ? activeClass : '', errorClass]"></div>
    ```
    > errorClass 会一直存在，但 activeClass 只会在 isActive 为真时才存在。

+ 然而，这可能在有多个依赖条件的 class 时会有些冗长。因此也可以在数组中嵌套对象：

    ```js
    <div :class="[{ active: isActive }, errorClass]"></div>
    ```

### 3. 在组件上使用​
+ 对于只有一个根元素的组件，当你使用了 class attribute 时，这些 class 会被添加到根元素上，并与该元素上已有的 class 合并

    > 举例来说，如果你声明了一个组件名叫 MyComponent，模板如下：

    ```js
    <!-- 子组件模板 -->
    <p class="foo bar">Hi!</p>
    ```
    > 在使用时添加一些 class：

    ```js
    <!-- 在使用组件时 -->
    <MyComponent class="baz boo" />
    ```
    > 渲染出的 HTML 为：

    ```js
    <p class="foo bar baz boo">Hi!</p>
    ```
    > Class 的绑定也是同样的：
    ```js
    <MyComponent :class="{ active: isActive }" />
    ```
    > 当 isActive 为真时，被渲染的 HTML 会是：
    ```js
    <p class="foo bar active">Hi!</p>
    ```
    > 如果你的组件有多个根元素，你将需要指定哪个根元素来接收这个 class。你可以通过组件的 $attrs 属性来实现指定：

    ```js
    <!-- MyComponent 模板使用 $attrs 时 -->
    <p :class="$attrs.class">Hi!</p>
    <span>This is a child component</span>
    ```
    ```js
    <MyComponent class="baz" />
    ```
    > 这将被渲染为：
    ```js
    <p class="baz">Hi!</p>
    <span>This is a child component</span>
    ```