# 菜单栏的封装

## index.vue

```html
<script setup>
import MenuPackaging from './MenuPackaging.vue';
import { reactive } from 'vue';
const menus = reactive([
    {
        title:'年级管理'
    },
    {
        title:'教师管理',
        children:[
            {
                title:"初级教师"
            },
            {
                title:"中级教师"
            },
            {
                title:"高级教师"
            },
        ]
    },
    {
        title:'学生管理'
    },
])
</script>
<template>
        <MenuPackaging :menuObj=menus></MenuPackaging>
</template>
```

## MenuPackaging.vue

```html
<script setup>
const props = defineProps(['menuObj'])
</script>
<template>
    <div>
        <el-menu>
            <template v-for="menu in menuObj">
                <el-sub-menu v-if="menu.children && menu.children.length>0">
                    <template #title>
                        {{menu.title}}
                    </template>
                    <!-- <template v-for="levelDown in menu.children">
                        <el-sub-menu v-if="levelDown.children && levelDown.children.length>0">
                            <template #title>
                                {{levelDown.title}}
                            </template>
                        </el-sub-menu>
                        <el-menu-item v-else>
                            {{levelDown.title}}
                        </el-menu-item>
                    </template> -->
                    <!-- 调用自己 -->
                    <MenuPackaging :menu-obj="menu.children"></MenuPackaging>
                </el-sub-menu>
                <el-menu-item v-else>
                    {{menu.title}}
                </el-menu-item>
            </template>
        </el-menu>
    </div>
</template>

```