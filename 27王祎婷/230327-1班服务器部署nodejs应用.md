# 服务器部署nodejs应用

## 一.必要条件

+ 1. 数据库:mysql

+ 2. node环境:nvm

+ 3. 代管工具:pm2

## 二.命令安装操作

+ 登录服务器

### 一.更新环境
+ 检查有没有要更新的
```js
    apt update
```
+ 更新
```js
    apt upgrade -y
```

### 二.安装数据库mysql
+ 看之前的笔记: https://gitee.com/xu-xiangqi1104/grade21_soft1_nodejs_log/blob/master/22%E8%AE%B8%E6%B9%98%E7%90%AA/230320-1%E7%8F%AD%E4%BA%91%E6%9C%8D%E5%8A%A1%E5%99%A8%E5%AE%89%E8%A3%85Mysql.md

+ 或者百度查找方法

+ exit退出数据库

### 三.安装nvm（只是部分，详情请看老师视频）

+ nvm是一个node的版本管理工具，可以简单操作node版本的切换、安装、查看。。。等等，与npm不同的是，npm是依赖包的管理工具。

+ 安装git:
```js
    apt install git
```

+ 改变颜色
 ```js 
  vim .bashrc
 ```
![1](./img/230327/1.png)

+ 1. 安装nvm的gitee镜像
    ```js
        git clone https://gitee.com/mirrors/nvm .nvm(重命名为 .nvm)
    ```
    + 查看目录
    ```js
        l
    ```

    + 进入nvm
        ```js
            cd .nvm 

            l
        ```
        ![2](./img/230327/2.png)

        + 给nvm.sh加上x权限
        ```js
            chmod 777 nvm.sh

            l
        ```
        ![3](./img/230327/3.png)

        + 输入nvm看看有没有下载成功
        ```js
            nvm
        ```

+ 2. cd退出当前文件，下载node长期支持版本
        ```js
            nvm install --lts
        ```
        ![4](./img/230327/4.png)

        + 查看node的版本
        ```js
            node -v
        ```
        ![5](./img/230327/5.png)

+ 3. 安装node的最新版本
        ```js
            nvm install node
        ```
        ![6](./img/230327/6.png)

+ 4. 查看都安装了那些版本
        ```js
            nvm list
        ```
        ![7](./img/230327/7.png)

+ 5. 切换版本
        ```js
            nvm use --lts
        ```
        ![8](./img/230327/8.png)

        + 查看是否有npm
        ```js
            npm -v
        ```

+ 6. 全局安装yarn
    ```js
         npm i -g yarn
    ```
    ![9](./img/230327/9.png)

    + 更新npm(也没有不用)
     ```js
         npm i -g npm
    ```

### 三.安装代管工具:pm2

+ 1. 安装pm2
```js
    npm i -g pm2
```

+ 2. 查看是否安装成功
    ```js
        pm2
    ```
    ![10](./img/230327/10.png)

    + 管理器(可以不搞)
    ```js
        pm2 monitor
    ```


## 三.部署项目

+ 1. 进入目录,将项目上传
    ```js
        cd /var/www
    ```
    + 克隆仓库
    ```js
        git clone 地址  front_back_end(重命名)
    ```
        
+ 2. 进入文件
    ```js
        cd front_back_end
    ```
+ 3. 进入后端文件
     ```js
        cd back_end
    ```
    + 安装
        ```js
            yarn
        ```
        ![11](./img/230327/11.png)

        + 如果仓库有修改，则推到上文件夹拉取,再yarn安装
        ```js
            cd ..

            git pull

            yarn
        ```
        ![12](./img/230327/12.png)
    
+ 4. 部署前端
        + 进入目录
        ```js
            cd /etc/nginx/conf.d
        ```
        + 查看底下文件
        ```js
            l
        ```

        + 设置首页的文件
        ```js
            vim 域名.conf

            //检查语法错误
             nginx -t

             nginx -s reload
        ```
+ 5. 回到后端路径，启动后端
    ```js
         pm2 start app.js
    ```

    + 查看运行状况
     ```js
        pm2 status
     ```

    + 使用反向代理，进入修改新增页面的窗口
    ```js
        vim 域名.conf
    ```
    ![14](./img/230327/15.png)


    + 将前端的地址全部改成代理的网站地址 

+ pm2的结束
        ```js
            pm2 stop all
        ```
        ![15](./img/230327/15.png)

## 四.项目部署的完成

网站: www.xq1104.top
    