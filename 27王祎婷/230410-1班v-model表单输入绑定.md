
#
## 一.v-model

+ v-model 可以自动将表单输入框的内容同步给 JavaScript 中相应的变量

+ v-model 还可以用于各种不同类型的输入，它会根据所使用的元素**自动**使用对应的 DOM 属性和事件组合

+ v-model 会忽略任何表单元素上**初始的** value、checked 或 selected attribute。它将始终将当前绑定的 JavaScript 状态视为数据的正确来源。应该在 JavaScript 中使用data 选项来声明该初始值。

### 代码:
```html
<!-- <script setup> 组合式-->
<script>

export default{
    data(){
      return {
        mess:'',
      }
    },
}

</script>

<template>
  <div>
    <!-- v-model：表单的绑定传值 -->
    <input type="text" v-model="mess">
    {{mess}}
  </div>

</template>
```


#
## 二.绑定

+ 文本框:
```html
    <td><input type="text" v-model="obj.name"></td>
```

+ 单选框:type中为 radio
```html
    <input type="radio" name="sex" value="f" v-model="obj.sex">女
    <input type="radio" name="sex" value="m" v-model="obj.sex">男
```

+ 复选框:type中为 checkbox
```html
    <input type="checkbox" name="hobby" value="篮球" v-model="obj.hobby">篮球
    <input type="checkbox" name="hobby" value="吃饭" v-model="obj.hobby">吃饭
```

+ 下拉框:包在**select 标签**中的**option 标签**，提供一个 **disabled 虚化禁用**掉对应的option 标签选项，可用于下拉框的顶部提示
```html
    <label for="">省: </label>
        <select v-model="obj.province.proname">
            <option value="" disabled>请选择省份</option>
            <template v-for="item of pro">
              <option :value="item">{{item}}</option>
            </template>
        </select>
```

#
## 三.代码
```html
<!-- js -->
<script>
  export default{
    data(){
      return{
        obj:{
          name:'',
          age:'',
          sex:'f',//性别
          hobby:[],
          province:
            {
              proname:'',
              city:'',
              counties:''
            },
        },
        //省
        pro:[
          '福建省','山东省','广东省'
        ],
        //省与市
        arr:[
          {
            proname:'福建省',
            city:['泉州市','龙岩市']
          },
          {
            proname:'山东省',
            city:['济南市','青岛市']
          },
          {
            proname:'广东省',
            city:['深圳市','广州市']
          }
        ],

        //市与县区
        count:[
          {
            city:'泉州市',
            counties:['鲤城区','丰泽区','晋江市']
          },{
            city:'龙岩市',
            counties:['新罗区','永定区','长汀县']
          },{
            city:'济南市',
            counties:['历下区','历城区','济阳区']
          },{
            city:'青岛市',
            counties:['城阳区','市南区','市北区']
          },{
            city:'深圳市',
            counties:['龙华区','宝安区','光明区']
          },{
            city:'广州市',
            counties:['天河区','海珠区','黄埔区']
          }
        ]
      }
    },
    methods:{
      but(){
        console.log(this.obj)
      },

      vif(item,name){ 
        if(item===name){
          return true
        }
        
      }
    }
  }
</script>

<!-- html -->
<template>
  <form>
    <table>
      <tr>
        <td><label for="">姓名</label></td>
        <td><input type="text" v-model="obj.name"></td>
      </tr>

      <tr>
        <td><label for="">年龄</label></td>
        <td><input type="text" v-model="obj.age"></td>
      </tr>

      <!-- 单选：radio -->
      <tr>
        <td><label for="">性别</label></td>
        <td>
          <input type="radio" name="sex" value="f" v-model="obj.sex">女
          <input type="radio" name="sex" value="m" v-model="obj.sex">男
        </td>
      </tr>

      <!-- 复选：checkbox -->
      <tr>
        <td><label for="">爱好:</label></td>
        <td>
          <input type="checkbox" name="hobby" value="篮球" v-model="obj.hobby">篮球
          <input type="checkbox" name="hobby" value="吃饭" v-model="obj.hobby">吃饭
          <input type="checkbox" name="hobby" value="睡觉" v-model="obj.hobby">睡觉
          <input type="checkbox" name="hobby" value="足球" v-model="obj.hobby">足球
        </td>
      </tr>

      <!-- 下拉框: select 
            默认:disabled
      -->
      <tr>
        <td><label for="">户籍</label></td>
        <td>
          <!-- 找到对应的省份，绑定到下拉框 -->
          <label for="">省: </label>
          <select v-model="obj.province.proname">
            <option value="" disabled>请选择省份</option>
            <template v-for="item of pro">
              <option :value="item">{{item}}</option>
            </template>
          </select>
          
          <!-- 根据选择的省份，找到对应的市进行下拉框绑定 -->
          <label for="">市: </label>
          <select v-model="obj.province.city">
            <option value="" disabled>请选择市</option>
            <template v-for="item of arr">
              <template v-if="vif(item.proname,obj.province.proname)">
                <option v-for="city of item.city" :value="city">{{city}}</option>
              </template>
            </template>
          </select>

          <!-- 根据选择的市，找到对应的县区进行下拉框的绑定 -->
          <label for="">县区: </label>
          <select v-model="obj.province.counties">
            <option value="" disabled>请选择县区</option>
            <template v-for="item of count">
              <template v-if="vif(item.city,obj.province.city)">
                <option v-for="counties of item.counties" :value="counties">{{counties}}</option>
              </template>
            </template>
          </select>
        </td>
      </tr>

    </table>
    <input type="button" value="提交" @click="but">
  </form>
</template>
```