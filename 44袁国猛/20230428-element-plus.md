

#### 安装 

###### yarn

```
   yarn add element-plus
```

###### npm

```
    npm install element-plus --save
```

###### pnpm

```
    pnpm install element-plus
```



#### main.js 文件中配置

```
//导入 element-pius 和相关文件
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'



const app=createApp(app)
app.use(ElementPlus)
app.mount('#app')
```

