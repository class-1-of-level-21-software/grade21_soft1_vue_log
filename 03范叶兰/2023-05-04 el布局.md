# el 布局

## 一.Container 布局容器

+ <**el-container**>：外层容器。 当子元素中包含 <**el-header**> 或 <**el-footer**> 时，全部子元素会垂直上下排列， 否则会水平左右排列。

+ <**el-header**>：顶栏容器。

+ <**el-aside**>：侧边栏容器。

+ <**el-main**>：主要区域容器。

+ <**el-footer**>：底栏容器

+ 在 <**style scopend**> 中，设置 **height:calc(100vh)** 可以使样式自适应撑开
```vue
<template>
  <div>
    <!-- el-container 外层容器。 -->
  <el-container>
    <!-- 侧边栏容器 -->
    <el-aside class="aside"><Nav></Nav></el-aside>
    <el-container>
      <!-- 顶栏容器 -->
      <el-header class="header">Header</el-header>
      <!-- 主要区域容器 -->
      <el-main class="main">
        <RouterView></RouterView>
      </el-main>
      <!-- 底栏容器 -->
      <el-footer class="footer">Footer</el-footer>
    </el-container>
  </el-container>
  </div>
</template>
<script setup>
import Nav from './Nav.vue'
</script>
<style scopend>
/* 测边栏的样式 */
.aside{
  background-color: rgb(163, 214, 104);
  /* 自适应 */
  height: calc(100vh);
}
/* 顶栏样式 */
.header{
  background-color: aqua;
}
/* 底栏样式 */
.footer{
  background-color: rgb(82, 72, 72);
  height: 500px;
}
</style>
```


## el-menu 导航栏设置

+ <**el-menu-item-group**> 组件可以实现菜单进行分组，分组名可以通过 title 属性直接设定，也可以通过具名 slot 来设定。

```vue
<template>

        <h5>导航栏</h5>
    <el-menu>
        <el-sub-menu index="1">
            <!-- 可以在下拉框的头显示 -->
            <template #title>
                <span>路由导航</span>
            </template>
            
            <el-menu-item-group title="路由">
            <el-menu-item index="1-1" @click="fn">Users</el-menu-item>
            <el-menu-item index="1-2" @click="fns">Student</el-menu-item>
          </el-menu-item-group>
        </el-sub-menu>

        <el-menu-item index="2">
            <span>
                <RouterLink to="/users">Users</RouterLink>
            </span>
        </el-menu-item>

        <el-menu-item index="3">
            <span>
                <RouterLink to="/student">Student</RouterLink>
            </span>
        </el-menu-item>
    </el-menu>
    
</template>

<script setup>
import {useRouter} from 'vue-router'
const router=useRouter();
const fn=function(){
    router.push({path:'/users'})
}
const fns=function(){
    router.push({path:'/student'})
}
</script>
```
