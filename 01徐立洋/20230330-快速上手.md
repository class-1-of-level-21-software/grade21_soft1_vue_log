# 快速上手
1. 创建vue项目
```
1. yarn create vite
2. npm create vue
3. npm init vue
```
2. v-bind，v-html试玩
```vue
<!-- HelloWorld.vue -->
<script setup>
defineProps({
  msg: {
    type: String,
    required: true
  },
  hh:String,
  uuid:Number,
  colorss:String
})
</script>

<template>
  <div class="greetings">
    <h1  v-bind:id="uuid" >{{ msg }}</h1>
    <h2 v-html="colorss"></h2>
    
  </div>
</template>

```
```vue
<!-- App.vue -->
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'
</script>

<template>
  <header>
    

    <div class="wrapper">
      <HelloWorld msg="呵呵哈哈哈" hh="那那那" uuid="16" colorss="<span style=color:orange>哈哈哈哈</span>"/>
      
    </div>
  </header>


</template>


```