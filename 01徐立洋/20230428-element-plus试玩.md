![show](./img/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-05-06%20144754.png)
![show](./img/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-05-06%20144725.png)

```js
// main.js
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

let app=createApp(App)
app.use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }


app.mount('#app')
```

```vue
<!-- App.vue -->
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import 'element-plus/theme-chalk/dark/css-vars.css'
import Icon from './components/icon.vue';
</script>

<template>
  <HelloWorld />
</template>


<!-- HelloWorld.vue -->
<template>

  <div class="common-layout">
    <el-container>
      <el-header>
        <el-menu :default-active="activeIndex" class="el-menu-demo" mode="horizontal" :ellipsis="false"
          @select="handleSelect">
          <el-menu-item index="0">LOGO</el-menu-item>
          <div class="flex-grow" />
          <el-menu-item index="1">Processing Center</el-menu-item>
          <el-sub-menu index="2">
            <template #title>Workspace</template>
            <el-menu-item index="2-1">item one</el-menu-item>
            <el-menu-item index="2-2">item two</el-menu-item>
            <el-menu-item index="2-3">item three</el-menu-item>
            <el-sub-menu index="2-4">
              <template #title>item four</template>
              <el-menu-item index="2-4-1">item one</el-menu-item>
              <el-menu-item index="2-4-2">item two</el-menu-item>
              <el-menu-item index="2-4-3">item three</el-menu-item>
            </el-sub-menu>
          </el-sub-menu>
        </el-menu>
      </el-header>
      <el-container>
        <el-aside width="200px">
          <el-tree :data="data" :props="defaultProps" accordion @node-click="handleNodeClick" />
        </el-aside>
        <el-container>
          <el-main>
            <el-skeleton />
            <br />
            <el-skeleton style="--el-skeleton-circle-size: 100px">
              <template #template>
                <el-skeleton-item variant="circle" />
              </template>
            </el-skeleton>
          </el-main>
          <el-footer>
            <span @click.stop="toggleDark()">暗黑模式</span>
            <el-switch size="small" v-model="isDark"></el-switch>
            <el-icon>
              <el-icon class="is-loading">
                <Loading />
              </el-icon>
              
            </el-icon>
            <Lock style="width: 1em; height: 1em; margin-right: 8px"/>
          </el-footer>
        </el-container>
      </el-container>
    </el-container>
  </div>
</template>



<script lang="ts" setup>
import { ref } from 'vue'
import { useDark, useToggle } from '@vueuse/core'

let color=ref('black')
let size=ref('66px')
const activeIndex = ref('1')
const handleSelect = (key: string, keyPath: string[]) => {
  console.log(key, keyPath)
}



interface Tree {
  label: string
  children?: Tree[]
}

const handleNodeClick = (data: Tree) => {
  console.log(data)
}

const data: Tree[] = [
  {
    label: 'Level one 1',
    children: [
      {
        label: 'Level two 1-1',
        children: [
          {
            label: 'Level three 1-1-1',
          },
        ],
      },
    ],
  },
  {
    label: 'Level one 2',
    children: [
      {
        label: 'Level two 2-1',
        children: [
          {
            label: 'Level three 2-1-1',
          },
        ],
      },
      {
        label: 'Level two 2-2',
        children: [
          {
            label: 'Level three 2-2-1',
          },
        ],
      },
    ],
  },
  {
    label: 'Level one 3',
    children: [
      {
        label: 'Level two 3-1',
        children: [
          {
            label: 'Level three 3-1-1',
          },
        ],
      },
      {
        label: 'Level two 3-2',
        children: [
          {
            label: 'Level three 3-2-1',
          },
        ],
      },
    ],
  },
]
const defaultProps = {
  children: 'children',
  label: 'label',
}
const value = ref(0)




const isDark = useDark()
const toggleDark = useToggle(isDark)
</script>

<style>
.el-header {
  /* background-color: rgb(19, 18, 18); */

}

.el-aside {
  /* background-color: gray; */
}

.el-main {
  background-color: rgb(248, 144, 162);

}

.el-footer {
  /* background-color: orange; */
}


.slider-demo-block {
  display: flex;
  align-items: center;
}

.slider-demo-block .el-slider {
  margin-top: 0;
  margin-left: 12px;
}

html.dark {
  /* 自定义深色背景颜色 */
  background-color: black;
  --el-bg-color: #626aef;
}
</style>




```