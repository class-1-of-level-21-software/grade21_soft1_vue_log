# 模板语法
```vue
<script setup>
import { ref } from 'vue'

defineProps({
  msg: String,
})

let uuid=19;//这是给v-bind用的
let panduan=false;//这是给v-show来判断用的，如果是true就会显示，如果是false就不会显示，if也是如此，如果这个常用就用v-show，如果不常用就用v-if

let color='<span style=color:red>哈哈哈</span>'//试试v-html

let a='id';
let s=0;
function abc() {
    console.log(s++);
    
}

</script>

<template>

<div :id="uuid"></div>
<!-- 这里 id 就是一个参数，它告诉 v-bind 指令将表达式 uuid 的值绑定到元素的 id attribute 上。在简写中，参数前的一切 (例如 v-bind:) 都会被缩略为一个 : 字符。 -->
<input type="button" :value="msg">
<div v-show="panduan">想藏起来</div>
<div v-if="panduan">我是if我也想藏起来</div>
<input type="button" value="点我试试" @click="abc" >
<div v-html="color"></div>
<div class="read-the-docs"> 呵呵</div>
 <p :[a]="s">哈哈哈</p>
 <!-- 这里的 a 会作为一个 JavaScript 表达式被动态执行，计算得到的值会被用作最终的参数。举例来说，如果你的组件实例有一个数据属性 a，其值为 "id"，那么这个绑定就等价于 v-bind:id。
相似地，你还可以将一个函数绑定到动态的事件名称上 -->
</template>

<style scoped>
.read-the-docs {
  color: red;
}
</style>

```