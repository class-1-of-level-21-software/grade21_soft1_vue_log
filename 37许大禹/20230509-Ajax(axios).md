# Vue3 Ajax(axios)

Axios是一个基于Promise的HTTP库，可以用在浏览器和node.js中。
### 安装方法
使用cdn:
```
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
```
或
```
<script src="https://cdn.staticfile.org/axios/0.18.0/axios.min.js"></script>
```
使用 npm:
```
$ npm install axios
```
使用 bower:
```
$ bower install axios
```
使用 yarn:
```
$ yarn add axios
```
使用方法：
```
Vue.axios.get(api).then((response) => {
  console.log(response.data)
})

this.axios.get(api).then((response) => {
  console.log(response.data)
})

this.$http.get(api).then((response) => {
  console.log(response.data)
})
```

## GET方法
简单的读取JSON数据：
```
const app = {
  data() {
    return {
      info: 'Ajax 测试!!'
    }
  },
  mounted () {
    axios
      .get('https://www.runoob.com/try/ajax/json_demo.json')
      .then(response => (this.info = response))
      .catch(function (error) { // 请求失败处理
        console.log(error);
    });
  }
}
 
Vue.createApp(app).mount('#app')
```
使用response.data读取JSON数据：
```
<div id="app">
  <h1>网站列表</h1>
  <div
    v-for="site in info"
  >
    { site.name }
  </div>
</div>
<script type = "text/javascript">
const app = {
  data() {
    return {
      info: 'Ajax 测试!!'
    }
  },
  mounted () {
    axios
      .get('https://www.runoob.com/try/ajax/json_demo.json')
      .then(response => (this.info = response.data.sites))
      .catch(function (error) { // 请求失败处理
        console.log(error);
    });
  }
}
 
Vue.createApp(app).mount('#app')
</script>
```
GET方法传递参数格式如下：
```
// 直接在 URL 上添加参数 ID=12345
axios.get('/user?ID=12345')
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
 
// 也可以通过 params 设置参数：
axios.get('/user', {
    params: {
      ID: 12345
    }
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
```
