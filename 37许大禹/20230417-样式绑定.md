# 样式绑定

## Vue.js class
class与 style是HTML元素的属性，用于设置元素的样式，我们可以用v-bind来设置样式属性。
v-bind 在处理class和style时， 表达式除了可以使用字符串之外，还可以是对象或数组。
v-bind:class可以简写为:class。

## class属性绑定 
可以为v-bind:class设置一个对象，从而动态的切换class:
```
将 isActive 设置为 true 显示了一个绿色的 div 块，如果设置为 false 则不显示：
<div :class="{ 'active': isActive }"></div>
```
div class渲染结果为：
```
<div class="active"></div>
```
也可以在对象中传入更多属性用来动态切换多个class 。
此外，:class指令也可以与普通的class属性共存。

```
text-danger 类背景颜色覆盖了 active 类的背景色：

<div class="static" :class="{ 'active' : isActive, 'text-danger' : hasError }">
</div>
```
div class渲染结果为：
```
<div class="static text-danger"></div>
```
当isActive或者hasError变化时，class属性值也将相应地更新。

也可以直接绑定数据里的一个对象：
```
text-danger 类背景颜色覆盖了 active 类的背景色：

<div id="app">
    <div class="static" :class="classObject"></div>
</div>
```
此外，我们也可以在这里绑定一个返回对象的计算属性。这是一个常用且强大的模式：
```
data() {
  return {
    isActive: true,
    error: null
  }
},
computed: {
  classObject() {
    return {
      active: this.isActive && !this.error,
      'text-danger': this.error && this.error.type === 'fatal'
    }
  }
}
```

### 数组语法
把一个数组传给v-bind:class 
```
<div class="static" :class="[activeClass, errorClass]"></div>
```
以上div class渲染结果为：
```
<div class="static active text-danger"></div>
```
还可以使用三元表达式来切换列表中的class ：
```
errorClass 是始终存在的，isActive 为 true 时添加 activeClass 类：

<div id="app">
    <div class="static" :class="[isActive ? activeClass : '', errorClass]"></div>
</div>
```
div class渲染结果为：
```
<div class="static text-danger"></div>
```