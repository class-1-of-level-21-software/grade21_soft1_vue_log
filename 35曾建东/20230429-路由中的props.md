# 说明
使用为组件配置路由时，可以配置对组件中的props进行传值
# 实例
```js
const routes = [
    {
        path:'/',
        components:XXX,
        //写法一：对象形式
        props:{x:'a'},
        //写法二：boolean 表示将所有的params参数注入到props中
        props:true,
        //写法三 函数形式，该函数可以收到一个route形参，是最为完备的写法，返回的对象注入props
        props(route){
            return {}
        },
        //多重结构
        props({params:{xx,xy},query:{yy,yx}}){
            
        }
        
    }
]
```