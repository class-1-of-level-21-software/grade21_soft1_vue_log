## 含义
在过去写网页是，常常用`网址?xxx=xxx&yy=yy`
这样的方式来传递参数。在vue-router中也同样可以做到

## 传递
```html
<router-link to='/index?a=1'>index</router-link>
```

## 接收
```js
this.$route.query.a
```