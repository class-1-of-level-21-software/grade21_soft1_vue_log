# 思路
实现数据的双向绑定的基础是，实现对数据的监听，
通过`Object.defineProperty`可以对对象的属性进行更多的定义
这里我们需要只需要它来定义getter和setter两个方法就ok了  
创建一个data对象 以及一个镜像的tdata对象向
我们通过getter方法使得data中的值始终等于tdata中的值
通过setter方法使得data中的值一旦发生改变就立即同时改变tdata中的值  

这样就实现了data与tdata之间的之间的双向绑定

为啥要这两东西绑定呢
是因为如果不用个东西与data的属性绑定，那么一旦定义它的setter就会造成一个堆栈溢出的问题

现在我们就得到了一个setter方法
在setter里面进行修改dom
利用这个方法我们可以实现数据的单项绑定

# 具体实现
```js
//数据源
var data = {}
//镜像数据源
var tdata = {}
//绑定方法
function bindData(id/*绑定的domID*/,name/*绑定的data中的属性名称*/,prop='value'/*绑定的Dom中的属性名称，默认为value */){

    //判断data中是否已经有该属性
    if(tdata[name] == undefined){
        tdata[name] = '';    //默认该属性为空字符串
        data[`${name}_setEns`]=[];      //定义在setter中执行事件盒子

        Object.defineProperty(data,name,{
            get:()=>{
                return tdata[name];
            },
            set:(value)=>{
                //让镜像属性 = 传递来的值
                tdata=value;
                //执行事件盒子中的事件
                data[`${name}_setEns`].forEach(e=>{
                    eval(e);
                })
            }
        })
        //实现data操作表单
        data[`${name}_setEns`].push(`document.getElementById('${id}').${prop} = tdata['${name}'];`);
            
        //定义监听表单的动作 实现表单操作data
        document.getElementById(id).addEventListener('keyup',function(e){data[name] = this[prop]});
        document.getElementById(id).addEventListener('keydown',function(e){data[name] = this[prop]});
    }
    
    data[`${name}_setEns`].push();
}
```
