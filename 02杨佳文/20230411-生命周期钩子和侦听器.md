# 生命周期钩子

生命周期钩子是在Vue对象生命周期的某个阶段执行的已定义方法

在整个生命周期内，总共分为8个阶段创建前/后，载入前/后，更新前/后，销毁前/后。

在选项式中，钩子一共有下面这些
```js
    beforeCreate //在组件实例初始化完成之后立即调用。

    created //在组件实例处理完所有与状态相关的选项后调用。

    beforeMount //在组件被挂载之前调用。

    mounted //在组件被挂载之后调用。

    beforeUpdate //在组件即将因为一个响应式状态变更而更新其 DOM 树之前调用。

    updated //在组件因为一个响应式状态变更而更新其 DOM 树之后调用。

    beforeUnmount //在一个组件实例被卸载之前调用。

    unmounted //在一个组件实例被卸载之后调用。

    errorCaptured //在捕获了后代组件传递的错误时调用。

    renderTracked //在一个响应式依赖被组件的渲染作用追踪后调用。

    renderTriggered //在一个响应式依赖被组件触发了重新渲染之后调用。

    activated //若组件实例是 <KeepAlive> 缓存树的一部分，当组件被插入到 DOM 中时调用。

    deactivated //若组件实例是 <KeepAlive> 缓存树的一部分，当组件从 DOM 中被移除时调用。

    serverPrefetch //当组件实例在服务器上被渲染之前要完成的异步函数。
```
在组合式中，除了没有beforeCreate、created两个方法以外，其他的钩子首字母大写并在前面加个on从vue中引入就能用
例如： 

[组合式onMounted](#onMounted)
## mounted

mounted是生命周期载入后的钩子，

在组件完成初始渲染并创建 DOM 节点后运行代码

### 选项式：
```vue
<script>
export default{
    mounted(){
    // 里面直接放要执行的语句
    console.log('泰裤辣');
  }
}
</script>
```
### 组合式：<a id="onMounted"></a>
```vue
<script setup>
import {onMounted} from 'vue'

onMounted(()=>{
    console.log('泰裤辣');
})
```

# 侦听器

计算属性允许我们声明性地计算衍生值。然而在有些情况下，我们需要在状态变化时执行一些“副作用”：例如更改 DOM，或是根据异步操作的结果去修改另一处的状态。

## watch
### 选项式

```vue
<script>
  import axios from 'axios'
export default{
  data(){
    return{
      question:'',
      answer:'',
      img:''
    }
  },
  watch:{
    question(newValue,oldValue){
        if ( newValue.endsWith('?') ) {
          this.answer='正在查找，请稍后....'
          axios.get('http://aa.jw0525.xyz/book').then(
            (res)=>{
              console.log(res);
              this.answer=res.data[3].bookname
              this.img=res.data[3].author
            }
          )
        }
    }
  }
}
</script>

<template>
<table>
  <tr>
    <td>问题：</td>
    <td><input type="text" v-model="question"></td>
  </tr>
  <tr>
    <td>答案：</td>
    <td><input type="text"  v-model="answer"></td>
  </tr>
  <tr>
    <td>图片：</td>
    <td>
      <img :src="img" alt="">
    </td>
  </tr>
</table>
</template>
```
####  this.$watch()
​
我们也可以使用组件实例的 $watch() 方法来命令式地创建一个侦听器：

```js
export default {
  created() {
    this.$watch('question', (newQuestion) => {
      // ...
    })
  }
}
```


### 组合式：

```vue
<script setup>
import axios from 'axios'
import {ref,watch} from 'vue'
//ref用于简单数据类型的属性，reactive用于复杂类型的属性，onMounted是组合式的mounted，是一个函数
let question=ref('')
let answer=ref('')
let img=ref('')
watch(question,async (newValue,oldValue)=>{
  if(newValue.endsWith('?')){
    answer.value='泰裤辣.....'
  }
  axios.get('http://aa.jw0525.xyz/book').then(
    (res)=>{
        answer.value=res.data[3].bookname,
         imga.value=res.data[3].author
    }
  )
})

</script>
```

效果图：

![watch](./img/watch.png)



## 停止侦听器
### 选项式

用 watch 选项或者 $watch() 实例方法声明的侦听器，会在宿主组件卸载时自动停止。因此，在大多数场景下，你无需关心怎么停止它。

在少数情况下，你的确需要在组件卸载之前就停止一个侦听器，这时可以调用 $watch() API 返回的函数：

```js
const unwatch = this.$watch('foo', callback)

// ...当该侦听器不再需要时
unwatch()
```
### 组合式
在 setup() 或 < script setup> 中用同步语句创建的侦听器，会自动绑定到宿主组件实例上，并且会在宿主组件卸载时自动停止。因此，在大多数情况下，你无需关心怎么停止一个侦听器。

一个关键点是，侦听器必须用同步语句创建：如果用异步回调创建一个侦听器，那么它不会绑定到当前组件上，你必须手动停止它，以防内存泄漏。如下方这个例子：
```vue

<script setup>
import { watchEffect } from 'vue'

// 它会自动停止
watchEffect(() => {})

// ...这个则不会！
setTimeout(() => {
  watchEffect(() => {})
}, 100)
</script>
```
要手动停止一个侦听器，请调用 watch 或 watchEffect 返回的函数：
```js

const unwatch = watchEffect(() => {})

// ...当该侦听器不再需要时
unwatch()
```
注意，需要异步创建侦听器的情况很少，请尽可能选择同步创建。如果需要等待一些异步数据，你可以使用条件式的侦听逻辑：
```js

// 需要异步请求得到的数据
const data = ref(null)

watchEffect(() => {
  if (data.value) {
    // 数据加载后执行某些操作...
  }
})
```
