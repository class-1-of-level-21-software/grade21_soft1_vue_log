# element-plus 的使用

官方文档: https://element-plus.gitee.io/zh-CN/


## 安装 element-plus 包管理器
```js
// yarn
     yarn add element-plus

// npm
    npm install element-plus --save

// pnpm
    pnpm install element-plus
```


## 全局引入配置

```js
// 在 main.js 文件中配置

//导入 element-pius 和相关文件
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app=createApp(app)
//全局配置
app.use(ElementPlus)

app.mount('#app')
```

## Container 布局容器

用于布局的容器组件，方便快速搭建页面的基本结构：

< el-container>：外层容器。 当子元素中包含 < el-header> 或 < el-footer> 时，全部子元素会垂直上下排列， 否则会水平左右排列。

< el-header>：顶栏容器。

< el-aside>：侧边栏容器。

< el-main>：主要区域容器。

< el-footer>：底栏容器。

### 简单布局

```vue

<template>
  <div class="common-layout">
    <el-container>
      <el-header>Header</el-header>
      <el-container>
        <el-aside width="200px">Aside</el-aside>
        <el-container>
          <el-main>Main</el-main>
          <el-footer>Footer</el-footer>
        </el-container>
      </el-container>
    </el-container>
  </div>
</template>

<style>
.el-header{
  background-color: red;
}
.el-aside{
  background-color: brown;
  height: 600px;
}
.el-main{
  background-color: blue;
}
.el-footer{
  background-color: yellow;
}
</style>

```
效果图：

![element](./img/element.png)