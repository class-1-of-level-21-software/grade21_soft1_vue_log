# menu菜单

为网站提供导航功能的菜单。

vue2和vue3使用的版本不相同，

有部分组件不一样导致功能使用出现问题，

例如，菜单的折叠功能，

element-ui的是el-submenu，

但到了vue3中，element-ui的组件无法识别，

需要用element-plus的el-sub-menu才能正常使用

# 侧栏

## el-menu

顾名思义，菜单，里面存放菜单列表

## el-menu-item

放在el-menu里，实现菜单项的呈现

## el-sub-menu

折叠菜单项 放在el-menu中，

需要< template #title> 辅助呈现标题

## 代码

```html
<template>
  <el-menu>
    <el-menu-item> 公司简介 </el-menu-item>
    <el-sub-menu>
      <template #title> 部门管理  </template>
      <el-menu-item>
        人事部
      </el-menu-item>
      <el-menu-item>
        设计部
      </el-menu-item>
      <el-menu-item>
        宣传部
      </el-menu-item>
      <el-menu-item>
        保安部
      </el-menu-item>
      <el-menu-item>
        公关部
      </el-menu-item>
      <el-sub-menu>
        <template #title>
            破事部
        </template>
        <el-sub-menu>
            <template #title>经理</template>
            <el-menu-item>胡强</el-menu-item>
        </el-sub-menu>
      </el-sub-menu>
    </el-sub-menu>
  </el-menu>
</template>

```

## 效果图

![el-menu](./img/el-menu.png)

