# 客户端 vs. 服务端路由

服务端路由指的是服务器根据用户访问的 URL 路径返回不同的响应结果。

当我们在一个传统的服务端渲染的 web 应用中点击一个链接时，浏览器会从服务端获得全新的 HTML，然后重新加载整个页面。

在单页面应用中，客户端的 JavaScript 可以拦截页面的跳转请求，动态获取新的数据，然后在无需重新加载的情况下更新当前页面。

这样通常可以带来更顺滑的用户体验，尤其是在更偏向“应用”的场景下，因为这类场景下用户通常会在很长的一段时间中做出多次交互

# VueRouter入门

Vue Router 是 Vue.js 的官方路由。

它与 Vue.js 核心深度集成，让用 Vue.js 构建单页应用变得轻而易举。

## 安装

```
npm install vue-router 或 yarn add vue-router
```

## router-link
请注意，我们没有使用常规的 a 标签，

而是使用一个自定义组件 router-link 来创建链接。

这使得 Vue Router 可以在不重新加载页面的情况下更改 URL，

处理 URL 的生成以及编码。

## router-view

router-view 将显示与 url 对应的组件。

你可以把它放在任何地方，以适应你的布局。

## 基本使用
main.js:
```js

import { createApp } from 'vue'
import App from './App.vue'
// 定义若干个组件并引入
import my from './components/My.vue'
import he from './components/He.vue'
import you from './components/You.vue'
// 安装vue-router 并引入需要的方法
import { createRouter, createWebHistory } from 'vue-router'

// 定义一个对象数组，里面放路由路径，和连接的组件
const routes = [
    { path: '/', component: my },
    { path: '/he', component: he },
    { path: '/you', component: you }
]

// 使用方法创建路由，history里放createWebHistory()，这会显示在路径上，定义的对象数组放上去
const router = createRouter(
    {
        history: createWebHistory(),
        routes
    }
)

// 创建app并使用刚刚创建的路由
createApp(App).use(router).mount('#app')
```

My.vue：

router-link是入口，最终会渲染成a链接

```html
<template>
    <router-link to="/you">你去</router-link>
    <router-link to="/he">他去</router-link>
    YJW
</template>
```

He.vue：

```html
<template>
     <router-link to="/">我去</router-link>
    JJJ
</template>
```

You.vue：

```html
<template>
    <router-link to="/">我去</router-link>
    HHH
</template>
```
APP.vue
APP.vue是出口，只需要放一个router-view，拥有显示三个组件的内容
```html
<template>
  <router-view></router-view>
</template>


```

效果：

当路径是http://localhost:5173/时，页面显示my组件的内容

渲染成了a链接标签

![router](./img/router2.png)

显示为：

![router2](./img/router.png)

点击你去，路径变成http://localhost:5173/you，页面显示you组件的内容

![router3](./img/router3.png)

点击我去会回到my，再点击他去，路径变成http://localhost:5173/he，页面的显示he组件的内容

![router4](./img/router4.png)

