```vue
 <el-menu
    :unique-opened="false"
    :default-active="routerActive"
    class="el-menu-vertical-demo"
    :collapse="store.iscollapse"
    text-color="#ffff"
    router
    @select="handleSelect"
    background-color="transparent"
    :collapse-transition="false"
  >

    <template v-for="menu in data.menuList" :key="menu.id">
      <el-menu-item
        :index="menu.name + ''"
        v-if="menu.children.length === 0"
        @click="handleClick(menu)"
      >
        <el-icon><i :class="'fa ' + menu.icon" aria-hidden="true"></i></el-icon>
        <template #title>{{ menu.title }}</template>
      </el-menu-item>
      <el-sub-menu v-else :index="menu.id + ''">
        <template #title>
          <el-icon><i :class="'fa ' + menu.icon" aria-hidden="true"></i></el-icon>
          <span>{{ menu.title }}</span>
        </template>
        <el-menu-item
          @click="handleClick(sub_menu)"
          :index="sub_menu.name"
          v-for="sub_menu in menu.children"
          :key="sub_menu.id"
          >{{ sub_menu.title }}</el-menu-item
        >
      </el-sub-menu>
    </template>
  </el-menu>
```

### 数据结构

```vue
const data = reactive({
      menuList: [
        {
          id: 1,
          icon: "fa-home",
          title: "首页",
          name: "home",
          children: [],
        },
        {
          id: 2,
          icon: "fa-user",
          title: "用户管理",
          children: [
            {
              id: 3,
              title: "用户列表",
              name: "userlist",
            },
          ],
        },
        {
          id: 4,
          icon: "fa-image",
          title: "图片管理",
          children: [
            {
              id: 5,
              title: "图片列表",
              name: "imagelist",
            },
          ],
        },
        {
          id:6,
          icon:'',
          title:'个人中心',
          children:[
            {
              id:7,
              title:"个人信息",
              name:"userinfo"
            }
          ]
        }
      ],
    });
```