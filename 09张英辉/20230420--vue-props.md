# props
app.vue
```
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import {ref,reactive,onMounted} from 'vue'

let arr=reactive([1,2,3,4])
onMounted(()=>{
  setTimeout(()=>{
    arr.push(18)
  },2000)
  console.log(arr);
})
</script>

<template>
  <HelloWorld name="张三" :age=arr  />
</template>
```
HelloWorld.vue
```
<script setup>
const props=defineProps({
  name:String,
  age:Array
})
</script>

<template>
<p>{{name}}</p>
<p>{{age}}</p>
</template>
```