##  一.安装 element-plus 包管理器

```
// yarn
     yarn add element-plus

// npm
    npm install element-plus --save

// pnpm
    pnpm install element-plus
```

# 

## 二.全局引入配置

- 在 main.js 文件中配置

```
//导入 element-pius 和相关文件
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

const app=createApp(app)
//全局配置
app.use(ElementPlus)

app.mount('#app')
```