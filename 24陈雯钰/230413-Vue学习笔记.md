# 侦听器和模板引用

## 一.watch函数 :侦听器

#
### 介绍:

+ 使用 watch 函数在每次响应式状态发生变化时触发回调函数

+ 能直接侦听响应式对象的属性值，需要用一个返回该属性的**函数**
```js
const obj = reactive({ count: 0 })
// 提供一个 getter 函数
watch(
  () => obj.count,
  (count) => {
    console.log(`count is: ${count}`)
  }
)
```
### 基础的内容:

+ 组合式
```html
<!-- 组合式 -->
<script setup>
import {ref,watch} from 'vue'
import axios from 'axios'

const question=ref('')
const name =ref('')
const imgUrl =ref('')

//定义侦听器(侦听的数据,匿名方法操作)
watch(question, async (newValue,oldValue)=>{
  //newValue:现在的值,oldValue：过去的值)
  // console.log(newValue)
  // console.log(oldValue)

  //判断现在的值里有没有要的字符
  if(newValue.endsWith('？') || newValue.endsWith('?')){
    name.value='正在查找相关信息，请稍等'
    console.log('开始查找')

    // 使用axios查看路由
    axios.get('http://localhost:3030/roles').then(res =>{

      //get暴露出来的
        console.log(res)

        let data=res.data.data;

        name.value=data.name
        imgUrl.value=data.img
    })
  }
})
</script>

<template>
  <div>
    请输入问题:
    <input type="text" v-model="question">
  </div>
  <div>
    答案：{{ name }}
  </div>
  <div>
    <img :src="imgUrl" alt="">
  </div>
</template>
```

+ 选项式
```html
<script>
import axios from 'axios'

export default{
    data(){
        //定义数据
        return{
            question:'',
            name:'',
            imgUrl:''
        }
    },
    //侦听器
    watch:{
        
        //侦听的数据方法
        // 每当 question 改变时，这个函数就会执行
        question(newValue,locValue){
            //判断是否有？,有？时执行
            if(newValue.endsWith('?')){
                //引入数据操作的异步方法
                this.getAll()
            }
        }
    },
    methods:{
        //异步函数方法
        async getAll(){
            this.name='正在查找答案，请稍等'

            //axios的路由方式
            axios.get('http://localhost:3030/roles').then(res=>{
                console.log('运行中')
                console.log(res)

                let data=res.data.data

                this.name=data.name
                this.imgUrl=data.img
            })
        }
    }

}
</script>

<template>
    <div>
        问题:
        <input type="text" v-model="question">
    </div>
    <div>
        回答:
        {{ name }}
    </div>
    <div>
        <img :src="imgUrl" alt="">
    </div>
</template>
```

+ 启动前后端分离，修改的代码
```js
async function getAll(ctx,next){
    // 随机生成一个整数范围在0~9
    let i=Math.floor(Math.random()*10)
    let res;
    if(i>5){
        res={
            code:1000,
            data:{
                name:'找到了',
                img:'https://img1.baidu.com/it/u=3904465785,574612916&fm=253&fmt=auto&app=138&f=JPEG?w=752&h=500'
            },
            msg:'获取成功'
        }
    }else{
        res={
            code:1000,
            data:{
                name:'没找到',
                img:'https://img1.baidu.com/it/u=1067723405,2531657160&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=586'
            },
            msg:'获取失败'
        }
    }
    ctx.body=res
}
```

#
## 二.ref: 模板引用

#
### 介绍

+ ref 是一个特殊的 attribute，它允许我们在一个特定的 DOM 元素或子组件实例被挂载后，获得对它的直接引用。

+ 只可以在组件挂载后才能访问模板引用。如果你想在模板中的表达式上访问 input，在初次渲染时会是 null。这是因为在初次渲染前这个元素还不存在

+ ref 数组并不保证与原始的数组有相同的顺序

### 基础的操作

+ 组合式
```html
<script setup>

import {ref, onMounted} from 'vue'

const div =ref(null)

//定义一个数组
const list=ref([1,2,3])
const items=ref([])

onMounted(()=>{
    console.log(div.value)
    console.log(items.value)

    //遍历元素被挂载后包含对应整个列表的所有元素
    items.value.forEach((i)=>{
        i.textContent
        console.log(i.textContent)
        
    })
})
</script>

<template>
    <div ref="div">
        <h1>111</h1>
    </div>

    <div>
        <ul>
            <!-- 循环数组 -->
            <li v-for="item of list" ref="items">
                {{ item }}
            </li>
            
        </ul>
    </div>
</template>
```

+ 选项式
```html
<script>
export default{
  data(){
    return{
      name:'',
      paw:'',
    }
  },

  //生命周期，初始渲染
  mounted(){
    console.log(this.$refs.div)
  },

  methods:{
    
    names(){
      this.name=this.$refs.name
      this.name.style="color : red"
      this.name.className='name'
      //focus()点击完按钮后，焦点会自动跳转到控件
      this.name.focus()
    },

    pawws(){
      this.paw=this.$refs.paw
      this.paw.style="color : red"
      this.paw.className='name'
      //focus()点击完按钮后，焦点会自动跳转到控件
      this.paw.focus()
    }
  }
}

</script>

<template>
  <div ref="div">
    <h1>111</h1>
  </div>
  <div>
    姓名: <input type="text" ref="name" >
  <input type="button" value="点击点击" @click="names">
  </div>
  <div>
    密码: <input type="text" ref="paw" >
  <input type="button" value="点击点击" @click="pawws">
  </div>
</template>
```