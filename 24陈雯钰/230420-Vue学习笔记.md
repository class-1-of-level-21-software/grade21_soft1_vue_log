# 深入Props

#
## 一.定义 Props 选项

+ 选项式的定义:

    + 没有使用 <**script setup**> 的组件方式为**选项式**，运用的是**选项式的API**

    + 在选项式中，prop 可以使用 **props 选项**来声明
```vue
<script>
export default{
    props:{
        xx:String,//字符串类型
        arr:Array,//数组类型
        judge:Boolean,//布尔值类型
        author:Object,//属性类型
        figure:Number
    }
}
</script>
```

+ 组合式的定义:

    + 使用 <**script setup**> 的单文件组件为**组合式**，运用的是**组合式的API**

    + 在组合式中，props 可以使用 **defineProps() 宏**来声明
```vue
<script setup>
import { ref } from 'vue'

defineProps({
  xx:String,
  arr:Array
})
</script>
```

+ 传递给 defineProps() 的参数和提供给 props 选项的值是相同的，两种声明方式背后其实使用的都是 **prop 选项**。

#
## 二.细节

### 命名格式

+ Prop 的命名格式使用 **camelCase 形式(小驼峰形式)**

+ 组件名 的命名格式使用 **PascalCase 形式**，因为这提高了模板的可读性，能帮助我们区分 Vue 组件和原生 HTML 元素。

### prop 的绑定

+ 直接使用 prop 的名字来进行静态形式的绑定
```html
<HelloWorld xx="111111"/>
```

+ 使用 **v-bind** 或**缩写 :** 来进行动态形式的绑定：当使用一个 JavaScript 表达式而不是一个字符串需要用到
```html
<HelloWorld :arr="arr"/>
```

### 传递不同类型

+ Number 类型:使用 **v-bind** 动态的绑定

+ Boolean 类型(布尔):

    + 不传值时，会隐性为 true ;
    ```html
    <Props judge />
    ```

    + 传值时，需要使用 **v-bind** 进行动态的绑定

+ Array 类型(数组):使用 **v-bind** 动态的绑定

+ Object 类型(属性):使用 **v-bind** 动态的绑定
```vue
<!-- Props.vue -->
<script>
export default{
    props:{
        xx:String,//字符串
        arr:Array,//数组
        judge:Boolean,//布尔值
        author:Object,//属性
        figure:Number
    }
}
</script>
<!-- App.vue -->
<template>
  <div>
    <Props 
    xx="1111" 
    :arr="arr" 
    judge 
    :author="{
      name:'222'
    }"
    :figure=12 />
  </div>
</template>
```

### Prop 校验


```vue
<!-- 选项式的校验 -->
<script>
export default{
    props:{
        propsA:{
            //基础类型检查
            type:String,
            //设置默认值
            default:'111'
        }
    }
}
</script>

<!-- 组合式的校验 -->
<script setup>
    defineProps({
        // 基础类型检查
        // （给出 `null` 和 `undefined` 值则会跳过任何类型检查）
        propA: Number,
        // Number 类型的默认值
        propD: {
            type: Number,
            default: 100
        }
    })
</script>

<template>
    <div>
        <h1>{{propsA}}</h1>
    </div>
</template>
```

## 三.其他细节

+ 所有 prop 默认都是可选的，除非声明了 required: true

+ Boolean 类型的未传递 prop 将被转换为 false。这可以通过为它设置 default 来更改

+ 如果声明了 default 值，那么在 prop 的值被解析为 undefined 时，无论 prop 是未被传递还是显式指明的 undefined，都会改为 default 值。