# 事件处理

详细网址 : https://cn.vuejs.org/guide/essentials/event-handling.html

## 一.v-on 监听事件

+ 使用 v-on 指令 (简写为 @) 来监听 DOM 事件，并在事件触发时执行对应的 JavaScript。

事件处理器 (handler) 的值可以是：

1. 内联事件处理器：事件被触发时执行的内联 JavaScript 语句 (与 onclick 类似)。

2. 方法事件处理器： v-on 也可以接受一个方法名或对某个方法的调用。

#
## 二.事件修饰符
+ v-on 提供了事件修饰符，使方法能更专注于数据逻辑而不用去处理 DOM 事件的细节

+ 修饰符是用 . 表示的指令后缀

修饰符包括:

1. .stop :单击事件停止向外传递

2. .prevent :提交事件将不再重新加载页面

3. .self :是元素本身时才会触发事件处理器

4. .capture :添加事件监听器时，使用捕获模式

5. .once :点击事件最多被触发一次

6. .passive :用于触摸事件的监听器，可以用来改善移动端设备的滚屏性能

```html
<!-- 单击事件将停止传递 -->
<a @click.stop="doThis"></a>

<!-- 提交事件将不再重新加载页面 -->
<form @submit.prevent="onSubmit"></form>

<!-- 修饰语可以使用链式书写 -->
<a @click.stop.prevent="doThat"></a>

<!-- 也可以只有修饰符 -->
<form @submit.prevent></form>

<!-- 仅当 event.target 是元素本身时才会触发事件处理器 -->
<!-- 例如：事件处理器不来自子元素 -->
<div @click.self="doThat">...</div>

<!-- 添加事件监听器时，使用 `capture` 捕获模式 -->
<!-- 例如：指向内部元素的事件，在被内部元素处理前，先被外部处理 -->
<div @click.capture="doThis">...</div>

<!-- 点击事件最多被触发一次 -->
<a @click.once="doThis"></a>

<!-- 滚动事件的默认行为 (scrolling) 将立即发生而非等待 `onScroll` 完成 -->
<!-- 以防其中包含 `event.preventDefault()` -->
<div @scroll.passive="onScroll">...</div>
```

#
## 三.按键

### 按键修饰符

+ Vue 允许在 v-on 或 @ 监听按键事件时添加按键修饰符。
```html
    <!-- 仅在 `key` 为 `Enter` 时调用 `submit` -->
    <input @keyup.enter="submit" />

```

### 按键别名

Vue 为一些常用的按键提供了别名：

1. .enter

2. .tab

3. .delete (捕获“Delete”和“Backspace”两个按键)

4. .esc

5. .space

6. .up

7. .down

8. .left

9. .right

### 系统按键修饰符

使用以下系统按键修饰符来触发鼠标或键盘事件监听器，只有当按键被按下时才会触发。

1. .ctrl

2. .alt

3. .shift

4. .meta
```html
<!-- Alt + Enter -->
<input @keyup.alt.enter="clear" />

<!-- Ctrl + 点击 -->
<div @click.ctrl="doSomething">Do something</div>

```

### .exact 修饰符

+ .exact 修饰符允许控制触发一个事件所需的确定组合的系统按键修饰符。
```html
<!-- 当按下 Ctrl 时，即使同时按下 Alt 或 Shift 也会触发 -->
<button @click.ctrl="onClick">A</button>

<!-- 仅当按下 Ctrl 且未按任何其他键时才会触发 -->
<button @click.ctrl.exact="onCtrlClick">A</button>

<!-- 仅当没有按下任何系统按键时触发 -->
<button @click.exact="onClick">A</button>

```

#
## 四.代码
```html
<!-- <script setup> 组合式-->
<script>

export default{
    data(){
      return {
        mess:'',
        mess1:'',
        arr:[
          {
            id:1,
            name:'111'
          },
          {
            id:2,
            name:'222'
          },
          {
            id:3,
            name:'333'
          },
        ],
        rev:[],
      }
    },
    methods:{
      abc(){
        console.log('2111')
      },
      a(){
        console.log('333')
      },
      ass(){
        console.log(this.mess)
        this.mess1=this.mess
      },
      rever(){
          this.rev=this.arr.reverse()
          
      }
    },
}

</script>

<template>
  <div>
    <!-- v-model：表单的绑定传值 -->
    <!-- 仅在按 `Enter`键 时调用 -->
    <input type="text" v-model="mess" @keyup.enter="ass">
    回车后的值:{{ mess1 }}
  </div>

  <!-- 数组的输出和条件的判断 -->
  <div>
    <template v-for="item of arr" :key="item.id">
      <ul>
      <li v-if="item.id%2===0">id被2整除的值:{{ item.name }}</li>
      <li v-else-if="item.id%3===0">id被3整除的值:{{ item.name }}</li>
    </ul>
    </template>
  </div>


  <!-- 按钮的设置 -->
  <div @click="a">
    <input type="button" value="按一下，会显示外层的信息" @click="abc">
    <!-- 单击事件将停止传递,不会向外传递显示 -->
    <input type="button" value="按一下,不会显示外层的信息" @click.stop="abc">
  </div>
  <div>
    <input type="button" value="倒叙" @click="rever">
    <template v-for="item of rev" :key="item.id">
      <p>id为{{ item.id }},值为{{ item.name }}</p>

    </template>
  </div>
</template>
```