# Vue 响应式基础


## 一.data函数

+ data 选项: 声明组件的响应式状态

+ data的值应返回一个**对象的函数**

+ 将函数返回的对象用响应式系统进行包装。此对象的所有顶层属性都会被代理到组件实例 (即方法和生命周期钩子中的 **this**) 上。

+ export default: 向外暴露的成员，可以使用任意变量来接收, **data函数放在 export default{}中**

```js
export default {
  data() {
    return {
      count: 0,
      text: '',
      awesome: true
    }
  },
}
```

#
## 二. methods对象

+ methods是一个**包含所有方法**的**对象**

+ Vue **自动**为 methods 中的方法**绑定了永远指向组件实例的 this**

+ 在**methods中不使用箭头函数**，因为箭头函数没有自己的 this 上下文。
```js
export default {
  data() {
    return {
      count: 0,
      text: '',
      awesome: true
    }
  },
}
  //methods 选项:是一个包含所有方法的对象
  methods: {
    //设置按钮的功能，增加
    incr() {
      this.count++
    },
  }
```

#
## 三.条件渲染:v-if,v-else

+ v-if :根据条件地渲染元素，依据true和falas来判断是否将 DOM 移除

+ v-else: 若v-if移除了DOM节点，v-else就会显示它管理的DOM节点
```html
    <p v-if="awesome">111</p>
    <!-- 若v-if移除了DOM节点，v-else就会显示它管理的DOM节点 -->
    <p v-else>111</p>
```

#
## 四.代码
```html
<script>

let id = 0;
//export default: 向外暴露的成员，可以使用任意变量来接收
export default {
  data() {
    return {
      count: 0,
      text: '',
      awesome: true
    }
  },
  //methods 选项:是一个包含所有方法的对象
  methods: {
    //设置按钮的功能，增加
    incr() {
      this.count++
    },
    //实时获取文本框的值
    textfn(e) {
      // target 事件属性获取最初发生事件的元素，返回触发事件的元素
      this.text = e.target.value
    },
    toggle() {
      //将数据改为反向的
      this.awesome = !this.awesome
    },
    
  },

}
// const count = ref(0)
</script>

<template>
  <!-- 文本框的显示 -->
  <div>
    {{ text }}
    <input type="text" v-model="text">
  </div>

  <!-- 按钮的设置 -->
  <div>
    <input type="button" value="玩了多久呢" @click="incr">{{ count }}
  </div>

  <div>
    <!-- 设置按钮的点击事件 -->
    <input type="button" value="按一下实现转换" @click="toggle">

    <!-- v-if :根据条件地渲染元素，根据true和falas来判断是否将 DOM 移除 -->
    <p v-if="awesome">111</p>
    <!-- 若v-if移除了DOM节点，v-else就会显示它管理的DOM节点 -->
    <p v-else>111</p>
  </div>
</template>

```