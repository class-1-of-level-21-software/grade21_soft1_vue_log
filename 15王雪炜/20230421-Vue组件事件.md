# 组件事件

## 触发与监听事件
* 在组件的模板表达式中，可以直接使用 $emit 方法触发自定义事件
* 像组件与 prop 一样，事件的名字也提供了自动的格式转换

```简单的一个组件事件
//子组件
<template>
  <div>
    <input type="button" value="我是一个组件单击按钮" @click="$emit('showTime')">
  </div>
</template>
//父组件
<script setup>
import HelloWorld from './components/HelloWorld.vue'

const op = function(){
  console.log('万花丛中过，片叶不沾身！');
}
</script>

<template>
<HelloWorld @showTime="op"/>
</template>

```
[![p9EUUnf.md.png](https://s1.ax1x.com/2023/04/21/p9EUUnf.md.png)](https://imgse.com/i/p9EUUnf)

```组合式
const emit = defineEmits(['showTime'])

const fn = function(){
  emit('showTime')
}
```
```选项式
<script>
export default {
  methods: {
    fn(){
      console.log('我是选线式的组件事件效果，请等待……');
      this.$emit('showTime')
    }
  }
}
</script>
```
[![p9EafMt.md.png](https://s1.ax1x.com/2023/04/21/p9EafMt.md.png)](https://imgse.com/i/p9EafMt)

## 事件参数
* 给$emit事件提供额外的参数

```
//父组件
const op = function(x,y,z){
  console.log('万花丛中过，片叶不沾身！');
  console.log('我是参数1：'+x);
  console.log('我是参数2：'+y);
  console.log('我是参数3：'+z);
}
//子组件
emit('showTime',222,555,888)
```
[![p9EdPJJ.md.png](https://s1.ax1x.com/2023/04/21/p9EdPJJ.md.png)](https://imgse.com/i/p9EdPJJ)

## 事件参数的累加
```
//父组件
let count=0;

const op = function (n) {
  console.log('万花丛中过，片叶不沾身！');
  count += n
  console.log('你已经点击了这个句子第'+count+'次');
}
//子组件
emit('showTime',1)
```
[![p9E0EVK.md.png](https://s1.ax1x.com/2023/04/21/p9E0EVK.md.png)](https://imgse.com/i/p9E0EVK)

```选项式
//父组件
<script>
export default {
  data() {
    return {
      count: 0
    }
  },
  methods: {
    add(x) {
      this.count += x
      console.log('诶嘿嘿，Nahida老婆 I LOVE YOU');
      console.log('你已经点击了第'+this.count+'次');
    }
  }
}
</script>

<template>
  <HelloWorld @clickMe="add" />
  <p>
    Nahida我已经想你废寝忘食第{{ count }}次
  </p>
</template>
//子组件
<script>
export default{
  
  methods:{
    btnClick(){
      console.log('我是选项式');
      this.$emit('clickMe',1)
    }
  }
}
</script>

<template>
  <div>
    <button @click="btnClick">轻点点我</button>
    
  </div>
</template>

```
[![p9ZOTkq.md.png](https://s1.ax1x.com/2023/04/23/p9ZOTkq.md.png)](https://imgse.com/i/p9ZOTkq)

## 登入
```
//子组件
<script setup>
import { reactive } from 'vue';

const emit = defineEmits({
  showTime({ user, password }) {
    if (user.trim() && password.trim()) {
      return true
    }
    console.warn('你的用户名或者密码错误，请检查后登入！！！');
    return false
  }
})

const role = reactive({
  user: '',
  password: ''
})

const btnSave = function () {
  emit('showTime', role)
}
</script>

<!-- <script>
export default {
  methods: {
    fn(){
      console.log('我是选线式的组件事件效果，请等待……');
      this.$emit('showT')
    }
  }
}
</script> -->

<template>
  <div>
    <table>
      <tr>
        <td>
          <label for="">用户名:</label>
        </td>
        <td>
          <input type="text" name="user" v-model="role.user">
        </td>
      </tr>
      <tr>
        <td>
          <label for="">密码:</label>
        </td>
        <td>
          <input type="password" name="password" v-model="role.password">
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <input type="button" value="提交保存" @click="btnSave">
        </td>
      </tr>
    </table>
  </div>
</template>

//父组件
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import { toRaw } from '@vue/reactivity'

const op = function (user, password) {
  console.log('欢迎你来到，万花丛中过，片叶不沾身！');
  
  console.log('用户名:'+toRaw(user).user);
  console.log('密码：'+toRaw(user).password);
  
}

</script>

<template>
  <HelloWorld @showTime="op" />
</template>
```
[![p9ErrZt.md.png](https://s1.ax1x.com/2023/04/21/p9ErrZt.md.png)](https://imgse.com/i/p9ErrZt)