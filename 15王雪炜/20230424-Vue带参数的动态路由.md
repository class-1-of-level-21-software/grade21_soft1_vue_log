# 带参数的动态路由

* 很多时候，我们需要将给定的匹配模式的路由映射到同一个组件。
* 路径参数：用冒号 : 表示
* 应该对所有用户进行渲染，但用户 ID 不同，我们可以在路径中使用一个动态字段来实现
* 当一个路由被匹配时，它的 params 的值将在每个组件中以 this.$route.params 的形式暴露出来。
* path地址里的id后面加一个'?'是可以在url里显示未有值，保证了路由的映射

```
//main.js
{ path: '/User/:id?', component: User }
//User.vue
<template>
    <br>
    <div>
        <h2>不打听打听提瓦特上的屑莹是谁？对就是本大爷</h2>
        <br>
        <RouterLink to="/">提瓦特(bushi)</RouterLink>
        <p></p>
        <RouterLink to="/Klee">去骑士团</RouterLink>
        <p></p>
        <RouterLink to="/Nahida">去净善宫</RouterLink>
        <p class="userId">请在URL上传入id?旅行者你的UID是：{{ this.$route.params.id }}</p>
    </div>
</template>
```

[![p9m09rd.md.png](https://s1.ax1x.com/2023/04/24/p9m09rd.md.png)](https://imgse.com/i/p9m09rd)
[![p9m0pKH.md.png](https://s1.ax1x.com/2023/04/24/p9m0pKH.md.png)](https://imgse.com/i/p9m0pKH)

## 响应路由参数的变化


##  createWebHashHistroy与createWebHistroy的区别

* Hash模式更适合开发模式，应为会在路由上加一个'#'号来区分，比较正常的url并不美观
* histo模式适合应用,他会显示完整的地址

## 捕获所有路由

```
//组件
<template>
    <div>
        <h1>你已到达了旅行的终点，却不知旅行的意义，旅行的初心又是什么？</h1>
        <h3>NotFound-404</h3>
        <br>
        <RouterLink to="/">Index</RouterLink>
    </div>
</template>
//main.js
import NotFound from './components/NotFound.vue'

{ path: '/:pathMatch(.*)', component: NotFound }
```

[![p9mB6kq.md.png](https://s1.ax1x.com/2023/04/24/p9mB6kq.md.png)](https://imgse.com/i/p9mB6kq)

