# Element-plus

* 目前仅有两个版本为Element-Plus和Element-UI
* Element-plus 以及 Element-UI 都是前端框架都有自己的风格组件库
* Element-Plus是采用了Vue 3.0的界面框架，而Element-UI是采用了Vue 2.0

## 类似的组件库

* 除了Element-plus等数据库，其有其他更多类似的组件库例如：
1. Bootstrap
2. Ant Design
3. Shards React
4. Prime React
5. Quasar 
6. Vuetify
7. Buefy
8. CoreUI Vue
9. Vuesax 
10. iView 

## 安装Element-Plus
* 使用包管理器，然后用你打包工具进行安装

```
# 选择一个你喜欢的包管理器

# NPM
$ npm install element-plus --save

# Yarn
$ yarn add element-plus

# pnpm
$ pnpm install element-plus
```

## 效果

[![p9l7oPs.md.png](https://s1.ax1x.com/2023/04/28/p9l7oPs.md.png)](https://imgse.com/i/p9l7oPs)