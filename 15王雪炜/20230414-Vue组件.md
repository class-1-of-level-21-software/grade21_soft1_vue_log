# 组件
* 组件允许我们将 UI 划分为独立的、可重用的部分，并且可以对每个部分进行单独的思考。在实际应用中，组件常常被组织成层层嵌套的树状结构：

```
 <script setup>

import { ref } from 'vue'

defineProps(['nico'])


</script> 

<script>
    export default{
        props:['nico']        
    }
</script>

<template>
    <div>
        <h1>{{ nico }}</h1>
    </div>
</template>

<template>
   <Bbab nico="创建选项式一个组件"></Bbab>
  <HelloWorld  />
</template>
```
[![ppzb1at.png](https://s1.ax1x.com/2023/04/14/ppzb1at.png)](https://imgse.com/i/ppzb1at)
[![ppzb3IP.png](https://s1.ax1x.com/2023/04/14/ppzb3IP.png)](https://imgse.com/i/ppzb3IP)

```
<template>
   <!-- <Bbab nico="创建响应式一个组件"></Bbab> -->
   <Bbab nico="创建选项式一个组件" ss="测试1" aa="测试b"></Bbab>
   <Bbab nico="创建选项式一个组件2" ss="测试1" aa="测试b"></Bbab>
   <Bbab nico="创建选项式一个组件3" ss="测试1" aa="测试b"></Bbab>
  <HelloWorld  />
</template>
```
* prop被注册后可以自定义，也可以重复使用

# 监听事件

## $emit
* 我们想要点击这个按钮来告诉父组件它应该放大所有博客文章的文字。要解决这个问题，组件实例提供了一个自定义事件系统。父组件可以通过 v-on 或 @ 来选择性地监听子组件上抛的事件，就像监听原生 DOM 事件那样

* 子组件可以通过调用内置的 $emit 方法，通过传入事件名称来抛出一个事件：

* 通过 emits 选项来声明需要抛出的事件：

* 这声明了一个组件可能触发的所有事件，还可以对事件的参数进行验证。同时，这还可以让 Vue 避免将它们作为原生事件监听器隐式地应用于子组件的根元素。

```
//Baba.vue

<script>
export default {
    props: ['nico', 'rain', 'simple'],
    data() {
        return {

        }
    }
}
</script>
<template>
    <div>
        
        <p>
            {{ nico }}
        </p>
        <p>
            {{ rain }}
        </p>
        <p>
            {{ simple }}
        </p>
        <input type="button" value="字体放大器" @click="$emit('big')">
        <br>
        <input type="button" value="字体缩小器" @click="$emit('small')">
    </div>
</template>

//App.vue

<script>
import Bbab from './components/Bbab.vue'
export default {
  data() {
    return {
      yy: 1,
    }

  },
  components: {
    Bbab
  },
  methods: {
    onClick() {
      this.yy += 0.3;

    },
    onClicks() {
      this.yy -= 0.2;
    }
  }
}
</script>

<template>
  <div :style="{ fontSize: yy + 'em' }">
    <Bbab nico="沙鹰3枪背身" simple="简单男孩" rain="雨很大" @big="onClick" @small="onClicks"></Bbab>
  </div>
</template>
```

[![ppzjPNn.png](https://s1.ax1x.com/2023/04/14/ppzjPNn.png)](https://imgse.com/i/ppzjPNn)
[![ppzjihq.png](https://s1.ax1x.com/2023/04/14/ppzjihq.png)](https://imgse.com/i/ppzjihq)