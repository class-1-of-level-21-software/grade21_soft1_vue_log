# 注册所有图标

* 您需要从 @element-plus/icons-vue 中导入所有图标并进行全局注册。

```
// main.ts

// 如果您正在使用CDN引入，请删除下面一行。
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
```

# Attributes

```
属性名：color 说明：svg 的 fill 颜色  类型：string    默认值：继承颜色
属性名：size    说明：SVG 图标的大小，size x size 类型：number / string   默认值：继承字体大小

```

# 旋转

```
<el-icon :class="abc" size="30" @click="iconHandle">
    <expand> </expand>
</el-icon>

import { ref, reactive, computed } from 'vue'
const isCollapse = ref(true);
const abc = reactive({
    collapseIcon: true
})
const iconHandle = function () {
    abc.collapseIcon = !abc.collapseIcon
    isCollapse.value = !isCollapse.value
}.collapseIcon {
    transform: rotate(-90deg);
}
```