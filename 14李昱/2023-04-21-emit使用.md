# 事件传参
```
emit作用是下级为了给上级反馈的
```
### 组合式
#### App.vue
```
<script setup>
import HelloWorld from './components/HelloWorld.vue'
let cjx = function(x){
  console.log(x);
}
</script>

<template>
  <HelloWorld @cjxdw="cjx"/>
</template>
```
#### HelloWorld.vue
```
<script setup>
const emit = defineEmits(['cjxdw'])
const fn = function() {
  emit('cjxdw','陈嘉鑫大王1')
}
</script>

<template>
<input type="button" value="确定" @click="fn">
</template>
```
#### 效果图
[![p9EatPJ.md.jpg](https://s1.ax1x.com/2023/04/21/p9EatPJ.md.jpg)](https://imgse.com/i/p9EatPJ)
# 声明触发的条件
```
组合式：
组件可以显式地通过 defineEmits() 宏来声明它要触发的事件
选项式：
组件可以显式地通过 emits 选项来声明它要触发的事件
```
### 注意
```
defineEmits() 宏不能在子函数中使用。如上所示，它必须直接放置在 <script setup> 的顶级作用域下

如果你显式地使用了 setup 函数而不是 <script setup>，则事件需要通过 emits 选项来定义
```
## 代码
```
HelloWorld.vue
<script setup>
const emit = defineEmits(['cjxdw','zdm'])
const fn = function() {
  emit('cjxdw','陈嘉鑫大王1')
}
const fn1 = function() {
  emit('zdm','zdm')
}
</script>

<template>
<input type="button" value="确定" @click="fn">
<input type="button" value="不确定" @click="fn1">
</template>

App.vue
<script setup>
import HelloWorld from './components/HelloWorld.vue'
let cjx = function(x){
  console.log(x);
}
let zdm = function(z){
  console.log(z); 
}
</script>

<template>
  <HelloWorld @cjxdw="cjx" @zdm="zdm"/>
</template>
```
# 事件校验
```
app.vue
<script setup>
import HelloWorld from './components/HelloWorld.vue'
function getVal(cjx,zdm){
  if(cjx && zdm){
    console.log(cjx,zdm);
  }
}
</script>

<template>
  <HelloWorld @submit="getVal"/>
</template>

helloworld
<script setup>
const emit = defineEmits({
  submit(cjx,zdm){
    if(cjx && zdm){
        return true
    }
    else{
      console.warn('没有值')
      return false
    }
  }  
});
let cjx = '';
let zdm = '';
function submitForm(){
  emit('submit',cjx,zdm)
}
</script>

<template>
<div><input type="text" v-model="cjx"></div>
<div><input type="text" v-model="zdm"></div>
<div><input type="button" value="确定" @click="submitForm"></div>
</template>
```