
完整网站: https://cn.vuejs.org/guide/essentials/template-syntax.html

## 一.文本插值

+ 最基本的数据绑定形式是文本插值，它使用的是“Mustache”语法 (即**双大括号**)

```html
<span>鸡腿: {{ msg }}</span>

```

## 二.原始HTML

+ 双大括号会将数据解释为纯文本，而不是 HTML。若想插入 HTML，你需要使用 **v-html 指令**：
```html
<p>鸡腿: {{ rawHtml }}</p>
<p>鸡腿: <span v-html="rawHtml"></span></p>

```

## 三.Attribute(属性) 绑定

+ v-bind 指令: Vue 将元素的 id attribute 与组件的 dynamicId 属性保持一致。如果绑定的值是 null 或者 undefined，那么该 attribute 将会从渲染的元素上移除。
```html
    <div v-bind:id="鸡腿"></div>
```

+ 简写为冒号:
```html
    <div :id="鸡腿"></div>

```