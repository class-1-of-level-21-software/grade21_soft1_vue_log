一、Element-Plus
HelloWorld.vue
```
<script setup>
defineProps({
  msg: {
    type: String,
    required: true
  }
})
</script>

<template>
  
</template>

<style scoped>

</style>
```

Home.vue
```
<script setup>
 import NavMenu from './NavMenu.vue';
</script>

<template>
    <div class="common-layout">
      <el-container>
        <el-header>Header</el-header>
        <el-container>
          <el-aside width="200px">
            Aside
          </el-aside>
          <el-container>
          <el-main>Main</el-main>
          <el-footer>Footer</el-footer>
          </el-container>
        </el-container>
      </el-container>
    </div>
  </template>

<style scoped>
.el-header{
    background-color: red;
}
.el-aside{
    background-color: blue;
    height: calc(100vh - 60px);
}
.el-main{
    background-color: rgb(58, 194, 204);
}
.el-footer{
    background-color: rgb(255, 0, 212);
}
</style>  
```

App.vue
```
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import Home from './components/Home.vue'
</script>

<template>
 <Home></Home>
</template>
```

main.js
```
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
// import './assets/main.css'
import 'element-plus/dist/index.css'
createApp(App).use(ElementPlus).mount('#app')
```