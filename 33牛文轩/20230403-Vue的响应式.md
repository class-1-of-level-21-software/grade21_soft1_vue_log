# 响应式
* 响应式是指：在不同宽度的屏幕下，都可以看到完整的页面内容，只不过，在宽度较小的屏幕下，可能内容会有删减，布局可能会有些改变等。响应式一般会使用 CSS3 媒体查询 @media 来实现。（PC端响应式多定义了几次CSS样式，然后在不同宽度的屏幕下显示出来，覆盖之前的CSS样式。）

## 声明响应式状态​
* 选用选项式 API 时，会用 data 选项来声明组件的响应式状态。此选项的值应为返回一个对象的函数。Vue 将在创建新组件实例的时候调用此函数，并将函数返回的对象用响应式系统进行包装。此对象的所有顶层属性都会被代理到组件实例 (即方法和生命周期钩子中的 this) 上。

## 代码
### Vue的组合式
```
<script setup>
import { ref } from 'vue'

defineProps({
  msg: String,

})


const count = ref(0)
function increment() {
  count.value++
  console.log(count.value);
}


</script>

<template>
  <button @click="increment">你已经点击了第：{{ count }}下</button>
  
</template>

```
![](https://s1.ax1x.com/2023/04/03/pphZexA.png)

### Vue的选项式
```
<script >

export default{
  data(){
    return{
      count:0
    }
  },
  methods:{
    increment(){
      this.count++
    }
  },
  mounted(){
    this.increment()
  }
}


</script>

<template>
  <button @click="increment">你已经点击了第：{{ count }}下</button>
  
</template>

```
![](https://s1.ax1x.com/2023/04/03/pphZBIU.png)