# 表单输入绑定

* 分为两种：第一种是传统的表单绑定，第二种是用简洁的v-model来输入表单绑定

```
<input
  :value="text"
  @input="event => text = event.target.value">


// v-model

<input v-model="text">

```


## 多行文本
```
<textarea v-model="message" placeholder="add multiple lines"></textarea>
```




```
<script setup>
import {toRaw} from '@vue/reactivity'
</script>
<script>

  export default{
    data(){
      return {
        user:{
          userName:'纳西妲',
          userAge:500,
          sex:'女',
          hobby:['sing','jump','rap','坤球'],
          address:'须弥',
          message:'请修改输入',
        }
      }
    },
    methods:{
      btnSave(){
       
        
        console.log(this.user.userName);
        console.log(this.user.userAge);
        console.log(this.user.sex);
        console.log(toRaw(this.user.hobby));
        console.log(toRaw(this.user.address));
        console.log(this.user.message);

        console.log('=====================================');
      }
    }
  }

</script>

<template>

  <table class="oo">
    <tr>
      <td><label for="">用户名：</label> </td>
      <td><input type="text" name="" id="" v-model="user.userName"></td>
    </tr>
    <tr>
      <td><label for="">年龄：</label></td>
      <td><input type="text" name="" id="" v-model="user.userAge"></td>
    </tr>
   <!-- 单选框 -->
    <tr>
      <td><label for="">性别：</label></td>
      <td><input type="radio" name="" id="" v-model="user.sex" value="男">男
        <input type="radio" name="" id="" v-model="user.sex" value="女">女
      </td>
    </tr>
    <!-- 复选框 -->
    <tr>
      <td><label for="">能力:</label></td>
      <td>
        <input type="checkbox" name="" id="" v-model="user.hobby" value="sing">唱
        <input type="checkbox" name="" id="" v-model="user.hobby" value="jump">跳
        <input type="checkbox" name="" id="" v-model="user.hobby" value="rap">rap
        <input type="checkbox" name="" id="" v-model="user.hobby" value="坤球">篮球
      </td>
    </tr>
    <!-- 下拉框 -->
    <tr>
      <td><label for="">住址:</label></td>
      <td>
        <select name="" id="" v-model="user.address"  >
          <option value="蒙德">蒙德</option>
          <option value="璃月">璃月</option>
          <option value="稻妻">稻妻</option>
          <option value="须弥">须弥</option>
        </select>
      </td>
    </tr>

    <tr>
      <td>角色介绍：</td>
      <td><textarea v-model="user.message" placeholder="add multiple lines"></textarea></td>
    </tr>

    <tr>
      <td></td>
      <td><input type="button" value="提交" @click="btnSave"></td>
    </tr>
  </table>

</template>

<style scoped>
.read-the-docs {
  color: #888;
}
.oo{
  text-align: left;
  
}

</style>


```
