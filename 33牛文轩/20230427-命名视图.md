一、编程式导航
Black.vue
```
<!-- <script setup>    

</script>

<template>
 <div  id="Blcak"></div>
 <h1>Black</h1>
 <RouterView></RouterView>
</template>


<style scoped>
    #Black {
        background-color: black;
        width: 250px;
        height:250px;
    }
  </style> -->
  ```

Blue.vue
```
<!-- <script setup>    

</script>

<template>
 <div  id="Blue"></div>
 <h1>Bule</h1>
 <RouterView></RouterView>
</template>


<style scoped>
    #Blue {
        background-color: blue;
        width: 250px;
        height:250px;
    }
  </style> -->
  ```

HelloWorld.vue
```
<!-- <script>
export default {
  created(){

  },
  methods: {
    abc(){
      console.log(333);
      this.$router.replace({name:'xy',params:{friends:'88'}})
    }

  }
}
</script>

<template>
  <input type="button" value="跳一跳" @click="abc">
</template>


<style scoped>
  #Blue {
      background-color: blue;
      width: 250px;
      height:250px;
  }
</style> -->


<script>

</script>

<template>
  
</template>
```

NavBar.vue
```
<template>
    <div id="nav">
       <RouterLink to="/setting/email">邮箱设置</RouterLink>
       <br>
       <RouterView to="/setting/profile">用户中心</RouterView>
    </div>
</template>
```
二、命名视图

UserEmail.vue
```
<template>
    <div id="email">
        电子邮箱
    </div>
</template>
```

UserEmailSub.vue
```
<template>
    <div>
        电子设备邮箱
    </div>
   
</template>
```

Userprofile.vue
```
<template>
    <div id="Preview">
        电子设备预览
    </div>
</template>
```

UserSetting.vue
```
<template>
    <div>
        用户设置
        <nav-bar></nav-bar>
        <RouterView></RouterView>
        <RouterView name="helper"></RouterView>
    </div>
</template>

<script setup>
    import NavBar from './NavBar.vue'
</script>
```

App.vue
```
<script setup>
import HelloWorld from './components/HelloWorld.vue';

</script>

<template>
<!-- <HelloWorld /> -->
<!-- 记住了，页面什么也没显示就是这里没加<RouterView></RouterView> -->
<RouterView></RouterView>   
</template>
```

main.js
```
// import { createApp } from 'vue'
// import App from './App.vue'
// import Black from './components/Black.vue'
// import Blue from './components/Blue.vue'
// import HelloWorld from './components/HelloWorld.vue'
// import {createRouter,createWebHistory} from 'vue-router'
// import './assets/main.css'

// const routes=[
//     {path:'/abc/:name?',component:HelloWorld },
//     {path:'/aac',component:Black },
//     {path:'/bbc/:friends?',name:'xy',component:Blue },
// ]
 
// const router=createRouter({
//     history:createWebHistory(),
//     routes
// })

// createApp(App).use(router).mount('#app')



 import { createApp } from 'vue'
 import App from './App.vue'
 import UserSetting from './components/UserSetting.vue'
 import UserEmailSub from './components/UserEmailSub.vue'
 import UserEmail from './components/UserEmail.vue'
 import Userprofile from './components/Userprofile.vue'
 import {createRouter,createWebHistory} from 'vue-router'
 import './assets/main.css'
const routes=[
    { path:'/',component:UserSetting },
  { path:'/setting',component:UserSetting,
    children:[
        {
            path:'email',
            component:UserEmailSub
        },
        {
            path:'profile',
            components:{ 
                default:UserEmail,
                helper:Userprofile
            } 
               

            
        }
    ] }
]
 
const router=createRouter({
    history:createWebHistory(),
    routes
})

createApp(App).use(router).mount('#app')
```