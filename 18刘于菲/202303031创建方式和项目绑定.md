第一种方法yarn创建项目
首先环境都先整好

node.js直接下一步就行：

下载官网:Node.js (nodejs.org)

1.首先全局安装webpack
npm install webpack -g

2.全局安装vue脚手架
npm install -g @vue/cli-init

3.安装Yarn
yarn比npm更快更高效是Facebook发布的node.js包管理器

安装命令:

npm i yarn -g -verbose

yarn安装之后把淘宝镜像给安装下

yarn config set registry https://registry.npm.taobao.org

注意vue和Yarn都需要配置环境变量

生成项目:

生成之前把环境换成: npm config set chromedriver_cdnurl https://npm.taobao.org/mirrors/chromedriver

 vue init webpack 项目名

安装依赖:

　#进入项目

　　cd 项目名

　　#安装依赖

　　yarn install

　　#安装完成之后启动

　　npm run dev

第二种方法使用vue/cli创建
vue create 项目名

可以先进入一个目录方便找到

cd D:

D盘创建一个cs的文件夹并进入

mkdir cs;

cd cs;

创建项目

vue  create项目名

 选着最后一个features自定义（空格选着，回车确定）

 选择插件

 选着vue版本

 步骤如下:

 创建完成

 ---------创建方式
CMD里面
 npm i yarn 
 yarn create vite
 选择vue js 
 创建文件名
 cd 文件名
 yarn
 yarn dev

 ---------项目绑定

 1 数据绑定
Vue中有两种数据绑定方式：

        1 单向绑定(v-bind)：数据只能从data流向页面。

        2 双向绑定（v-modle）：数据不仅能从data流向页面，还能从页面流向data

 注意：

1双向绑定一般都应用在表单项元素上（如：input、select）

2.v-modle: value可以简写v-modle，因为v-modle默认收集的就是value值

Attribute 绑定#
双大括号不能在 HTML attributes 中使用。想要响应式地绑定一个 attribute，应该使用 v-bind 指令：

template
<div v-bind:id="dynamicId"></div>
v-bind 指令指示 Vue 将元素的 id attribute 与组件的 dynamicId 属性保持一致。如果绑定的值是 null 或者 undefined，那么该 attribute 将会从渲染的元素上移除。

简写#
因为 v-bind 非常常用，我们提供了特定的简写语法：

template
<div :id="dynamicId"></div>
开头为 : 的 attribute 可能和一般的 HTML attribute 看起来不太一样，但它的确是合法的 attribute 名称字符，并且所有支持 Vue 的浏览器都能正确解析它。此外，他们不会出现在最终渲染的 DOM 中。简写语法是可选的，但相信在你了解了它更多的用处后，你应该会更喜欢它。

接下来的指引中，我们都将在示例中使用简写语法，因为这是在实际开发中更常见的用法。
 
