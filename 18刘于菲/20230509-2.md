路径 在你需要的页面引入组件 嵌套在div里面可以避免不必要的错误

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<template>
  <div >     
  <el-menu  default-active="this.$router.path" class="el-menu-vertical-demo" @open="handleOpen" @close="handleClose" :collapse="isCollapse"
                  mode="vertical"
                  background-color="#2c2e2f"
                  text-color="#bfcbd9"
                  active-text-color="#85c1e7"
                  :default-active="activeIndex"
                  router>
          <NavMenu :navMenus="sret"></NavMenu>
        </el-menu>
<router-view :key="$route.path + $route.query.t"></router-view>
:key="$route.path + $route.query.t" //解决跳转到相同路由页面 数据不刷新问题
 
</div> </template>
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<script type="text/ecmascript-6">
  import NavMenu from "@/components/NavMenu" //组件位置
export default {
  components: { //定义组件
    NavMenu,
  },
  name: 'login',
  data() {
    return {}
　　}
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
:navMenus="sret" 这里的sret是后台拿过来的JSON树状数据 数据要转换成树状的 不然不行  吧转换后的数据赋值sret


让路由在<router-view></router-view>打开

定义路由和跳转的页面

路径 router/index.js 里面

```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
需要几个页面就按这样的跳转 都要引入
import Ibookmark from '@/views/bookmark';
..........
// 启用路由
Vue.use(Router);

// 导出路由 
export default new Router({
routes: [{
  path: '/login',
  name: '',
  component: login,
  children: [ 
　　　　{
  path: '/bookmark/:menuId',
  component: resolve => require(['../views/bookmark.vue'], resolve),
  query:{
    t:Date.now(), //解决跳转到相同路由页面 数据不刷新问题
  },
 
},
  ],
},........省略
 
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

父子路由 是层级关系 children: []