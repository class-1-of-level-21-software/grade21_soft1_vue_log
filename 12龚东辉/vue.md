渐进式JavaScript框架

    Vue.js优点
        体积小
        更高的运行效率
        基于虚拟dom，一种可以预先通过JavaScript进行各种计算，把最终的DOM操作计算出来病优化的技术，由于这个DOM操作属于预处理操作，并没有真实的操作DOM。
        双向数据绑定
        让开发者不要再去操作dom独享，把更多的精力投入到业务逻辑上。
        生态丰富，学习成本低

Vue.js 全段开发人员必修技能
被广泛的应用于web端，移动端，跨平台应用开发。

    安装与部署

第一种安装方式（简单）：
直接下载并用<script>标签引入，Vue会被注册为一个全局变量。

NPM
在用Vue构建大型应用时推荐使用NPM安装。NPM能很好地和诸如webpack或Browserify模块打包器配合使用。同时Vue也提供配套工具来开发单文件组件。
安装命令：
npm install
 vue

第二种安装方式：
命令行工具（CLI）
Vue提供了一个官方的CLI，为单页面应用（SPA）快速搭建复杂的脚手架。

Vude.js有开发版本/生产版本。

引入：
<script src="vue.js" type="text/javascript" charset="utf-8"></script>

引入成功，会暴露一个全局的变量，Vue()

第二种引入方式：CDN方式
<script src="https://cdn.jsdelivr.net/npm/vue"></script>

    创建第一个vue应用

声明式渲染
Vue.js的核心是一个允许 采用简洁的模板语法来声明式地将数据渲染进DOM的系统。
Vue.js的应用分为两个重要的部分，一个是视图，一个是脚本。

视图部分，比如使用div标签来定义（指定一个id值），然后使用左右双大括号{{}}来声明变量。
脚本部分，使用script标签来定义，使用new Vuew() 来返回一个应用。
其中，el表示元素element，并使用id选择器（#号开头）来选中视图部分的div。
第二个为data，表示数据，用户保存数据。我们在视图中声明了哪些变量，就需要在data中进行注册并初始化赋值。

HBuilderX 预览：

    数据与方法

Vue实例
创建一个Vue实例：
每个Vue应用都是通过Vue函数创建一个新的Vue实例开始的：
var vm = new Vue({
    //选项
})

当一个Vue实例被创建时，它将data对象中的所有属性加入到Vue的响应式系统中。当这些属性的值发生改变时，视图将会产生“响应”，即匹配更新为新的值。
当这些数据改变时看，视图会进行重渲染。值得注意的是只有当实例被创建时data中存在的属性才是响应式的。

对变量使用Object.freeze()，这会阻止修改现有的属性，也意味着相应系统无法再追踪变化。

除了数据属性，Vue实例还暴露了一些有用的实例属性与方法。它们都有前缀，以便与用户定义的属性区分开来。watch是一个实例方法，可以观察一个变量的变化，并且获取变化前/后的结果。
该方法第一个参数为需要观察的变量，第二个参数为一个回调，回调参数的参数分别为newVal和oldVal。

    生命周期

每个Vue实例在被创建时都要经过一系列的初始化过程，需要设置数据监听、编译模板、将实例挂载到DOM并在数据变化时更新DOM等。同时在这个过程中也会晕习惯一些叫做生命周期钩子的函数，这给了用户在不同阶段添加自己的代码的机会。

比如 creatd钩子可以用来在一个实例被创建之后执行代码。
也有一些其他的钩子，在实例生命周期的不同阶段调用，如mounted、updated和destroyed，生命周期钩子的this上下文执行调用它的Vue实例。

注意：
不要在选项属性或回调上使用箭头函数，比如 created: () => console.log(this.a) 或 vm.$watch('a', newValue => this.myMethod())。因为箭头函数并没有this，this会作为变量一直向上级词法作用域查找，直到找到为止，经常导致Uncaught TypeError：Cannot read property of undefined 或 Uncaught TypeError：this.myMethod is not a function 之类的错误。

扩展：
点击官网上的学习，可以进入 选项/生命周期钩子，学习生命周期API介绍。

API：
beforeCreate 在实例初始化之后，数据观测（data observer）和event/watcher事件配置之前被调用。
created 在实例创建完成后被立即调用。在这一步，实例已完成以下的配置：数据观测（data observer），属性和方法的运算，watch/event 事件回调。然而，挂载阶段还没开始，el 属性目前不可见。 beforeMount 在挂载开始之前被调用加，相关的渲染函数首次被调用。 mounted el 被新创建的 vm.el 替换，挂载成功。
beforeUpdate 数据更新时调用
updated 组件DOM已经更新，组件更新完毕。

设置类似定时器或者延时函数方法：如下是在3秒之后更新msg变量的数据。
