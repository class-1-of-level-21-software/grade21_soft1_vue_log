## 一、侦听器
```html
<script setup>
import {ref,reactive,onMounted,watch} from 'vue'
import axios from 'axios'

let question=ref('');
let answer=ref('');
watch(question,async(newValue,oldValue)=>{
    if(newValue.endsWith('?')){
      console.log('问题成型了!');
      answer.value='正在查询，请稍候。。。'

      axios.get('https://yesno.wtf/api').then(res=>{
          console.log(res);
          let data=res.data;
          console.log(data);
          answer.value=data.answer;
          console.log(answer);
      })
    }
})
// 一、
// onMounted(()=>{
//   console.log('不知名王友');
// })

// let nickname=ref('项羽');
// let stu=reactive({
//   id:2,
//   name:'虞姬'
// })


</script>

<template>
  <!-- 一、
  <div>
    <input type="text" v-model="nickname">
    {{ nickname }}
    <input type="text" v-model="stu.name">

  </div> -->
  <p>
    问题:
  <input type="text" v-model="question">
  </p>


<p>
  回答如下:{{ answer }}
</p>


</template>
```

## 二、模板引用
```html
<script setup>
import {ref,reactive,onMounted,watch} from 'vue'
const ali=ref(null);
const p4=ref('');
onMounted(()=>{
  // console.log(ali.value.focus());
  console.log(ali.value);
})
</script>

<template>
  <div ref="p4">
    <!-- <p ref="ali">1688</p> -->
    <p :ref="(item)=>{ali=item}">1688</p>
  </div>
</template>
```
