## 一、Element-Plus导航栏
### HelloWorld.vue
```html
<script setup>
defineProps({
  msg: {
    type: String,
    required: true
  }
})
</script>

<template>
  
</template>

<style scoped>

</style>
```
### Home.vue
```html
<script setup>
 import NavMenu from './NavMenu.vue';
</script>

<template>
    <div class="common-layout">
      <el-container>
        <el-header>Header</el-header>
        <el-container>
          <el-aside width="200px">
            <NavMenu></NavMenu>
          </el-aside>
          <el-container>
          <el-main>Main</el-main>
          <el-footer>Footer</el-footer>
          </el-container>
        </el-container>
      </el-container>
    </div>
  </template>

<style scoped>
.el-header{
    background-color: red;
}
.el-aside{
    background-color: blue;
    height: calc(100vh - 60px);
}
.el-main{
    background-color: rgb(58, 194, 204);
}
.el-footer{
    background-color: rgb(255, 0, 212);
}
</style>  
```

### NavMenu.vue
```html
<template>

<el-radio-group v-model="isCollapse" style="margin-bottom: 20px;">
  <el-radio-button :label="false">展开</el-radio-button>
  <el-radio-button :label="true">收起</el-radio-button>
</el-radio-group>
<el-menu default-active="1-4-1" class="el-menu-vertical-demo" @open="handleOpen" @close="handleClose" :collapse="isCollapse">
  <el-submenu index="1">
    <template slot="title">
      <i class="el-icon-location"></i>
      <span slot="title">导航一</span>
    </template>
    <el-menu-item-group>
      <span slot="title">分组一</span>
      <el-menu-item index="1-1">选项1</el-menu-item>
      <el-menu-item index="1-2">选项2</el-menu-item>
    </el-menu-item-group>
    <el-menu-item-group title="分组2">
      <el-menu-item index="1-3">选项3</el-menu-item>
    </el-menu-item-group>
    <el-submenu index="1-4">
      <span slot="title">选项4</span>
      <el-menu-item index="1-4-1">选项1</el-menu-item>
    </el-submenu>
  </el-submenu>
  <el-menu-item index="2">
    <i class="el-icon-menu"></i>
    <span slot="title">导航二</span>
  </el-menu-item>
  <el-menu-item index="3" disabled>
    <i class="el-icon-document"></i>
    <span slot="title">导航三</span>
  </el-menu-item>
  <el-menu-item index="4">
    <i class="el-icon-setting"></i>
    <span slot="title">导航四</span>
  </el-menu-item>
</el-menu>  
</template>
  <style>
    .el-menu-vertical-demo:not(.el-menu--collapse) {
      width: 200px;
      min-height: 400px;
    }
  </style>
  
  <script>
    export default {
      data() {
        return {
          isCollapse: true
        };
      },
      methods: {
        handleOpen(key, keyPath) {
          console.log(key, keyPath);
        },
        handleClose(key, keyPath) {
          console.log(key, keyPath);
        }
      }
    }
  </script>
```

### App.vue
```html
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import Home from './components/Home.vue'
</script>

<template>
 <Home></Home>
</template>

```

### main.js
```js
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
// import './assets/main.css'
import 'element-plus/dist/index.css'
createApp(App).use(ElementPlus).mount('#app')
```