## 一、路由
### About.vue
```html
<script setup>
import { RouterView } from 'vue-router';
</script>

<template>
    <div>
       <!-- <RouterLink to="/">我要去Home啊</RouterLink> -->
       <!-- <RouterView>55</RouterView> -->
        我是一个小小的About
        <RouterLink to="/">归家了</RouterLink>  
    </div>
</template>
```

### Center.vue
```html
<template>
  <div>
    巴拉巴拉毛
  </div>
</template>
```

### HelloWorld.vue
```html
<script>
import { RouterLink } from 'vue-router'
export default {

  components:{
    RouterLink
  }
}
</script>

<template>
 <div>
  <router-link to="/">88</router-link>
  <br>
    <router-link to="/about">99</router-link>
 </div>
</template>
```

### Home.vue
```html
<script setup>
// import { RouterView,RouterLink } from 'vue-router'
</script>

<template>
    <div>
        <RouterLink to="/about">我要去About啊</RouterLink>
        <RouterLink to="/center">我要去Center啊</RouterLink>
        <!-- <RouterView></RouterView> -->
       <h1>我是一个大大的Home</h1>   
    </div>
</template>
```

### App.vue
```html
<!-- <script>
import Home from './components/Home.vue'
// import { RouterView } from 'vue-router';
export default {
  components: {
    Home
  }
}
</script> -->

<template>
  <RouterView></RouterView>
</template>
```

### main.js
``` js
import { createApp } from 'vue'
import './assets/main.css'
import App from './App.vue'
import { createRouter,createWebHashHistory } from 'vue-router'
import Home from './components/Home.vue'
import About from './components/About.vue'
import Center from './components/Center.vue'

// 1. 定义路由组件.
// 也可以从其他文件导入
// const Home = { template: '<div>Home</div>' }
// const About = { template: '<div>About</div>' }

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About },
  { path: '/center', component:Center}
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})
 
createApp(App).use(router).mount('#app')
```