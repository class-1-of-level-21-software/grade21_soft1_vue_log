Vue 组件基础和深入组件注册
===
一个 Vue 组件在使用前需要先被“注册”，这样 Vue 才能在渲染模板时找到其对应的实现。组件注册有两种方式：全局注册和局部注册

一 .动态组件：
---
+ 有些场景会需要在两个组件间来回切换，比如 Tab 界面：
    > 通过 Vue 的 <component> 元素和特殊的 is attribute 实现的：
    ```html
    <!-- currentTab 改变时组件也改变 -->
    <component :is="currentTab"></component>
    ```
    > 在上面的例子中，被传给 :is 的值可以是以下几种：
    + 被注册的组件名
    + 导入的组件对象
    > 你也可以使用 is attribute 来创建一般的 HTML 元素

    > 当使用 `<component :is="...">` 来在多个组件间作切换时，被切换掉的组件会被卸载。我们可以通过 `<KeepAlive>` 组件强制被切换掉的组件仍然保持“存活”的状态

二 .DOM 模板解析注意事项：
---
+ 如果你想在 DOM 中直接书写 Vue 模板，Vue 则必须从 DOM 中获取模板字符串。由于浏览器的原生 HTML 解析行为限制，有一些需要注意的事项
    >请注意下面讨论只适用于直接在 DOM 中编写模板的情况。如果你使用来自以下来源的字符串模板，就不需要顾虑这些限制了：
    1. 单文件组件
    2. 内联模板字符串 (例如 template: '...')
    3. `<script type="text/x-template">`

### 1. 大小写区分​
> HTML 标签和属性名称是不分大小写的，所以浏览器会把任何大写的字符解释为小写。这意味着当你使用 DOM 内的模板时，无论是 PascalCase 形式的组件名称、camelCase 形式的 prop 名称还是 v-on 的事件名称，都需要转换为相应等价的 kebab-case (短横线连字符) 形式：
```js
// JavaScript 中的 camelCase
const BlogPost = {
  props: ['postTitle'],
  emits: ['updatePost'],
  template: `
    <h3>{{ postTitle }}</h3>
  `
}
template
<!-- HTML 中的 kebab-case -->
<blog-post post-title="hello!" @update-post="onUpdatePost"></blog-post>
```
### 2. 闭合标签​
> 我们在上面的例子中已经使用过了闭合标签 (self-closing tag)：
```html
<MyComponent />
```
> 这是因为 Vue 的模板解析器支持任意标签使用 /> 作为标签关闭的标志

> 然而在 DOM 模板中，我们必须显式地写出关闭标签：
```html
<my-component></my-component>
```

> 这是由于 HTML 只允许一小部分特殊的元素省略其关闭标签，最常见的就是 `<input>`和 `<img>`。对于其他的元素来说，如果你省略了关闭标签，原生的 HTML 解析器会认为开启的标签永远没有结束，用下面这个代码片段举例来说：
```html
<my-component /> <!-- 我们想要在这里关闭标签... -->
<span>hello</span>
```
> 将被解析为：
```html
<my-component>
  <span>hello</span>
</my-component> <!-- 但浏览器会在这里关闭标签 -->
```

### 3. 元素位置限制​
+ 某些 HTML 元素对于放在其中的元素类型有限制，例如 `<ul>，<ol>，<table> 和 <select>`，相应的，某些元素仅在放置于特定元素中时才会显示，例如 `<li>，<tr> 和 <option>`

> 这将导致在使用带有此类限制元素的组件时出现问题。例如：
```html
<table>
  <blog-post-row></blog-post-row>
</table>
```
> 自定义的组件 `<blog-post-row>` 将作为无效的内容被忽略，因而在最终呈现的输出中造成错误。我们可以使用特殊的 is attribute 作为一种解决方案：
```html
<table>
  <tr is="vue:blog-post-row"></tr>
</table>
```

##### 注意：当使用在原生 HTML 元素上时，is 的值必须加上前缀 vue: 才可以被解析为一个 Vue 组件。这一点是必要的，为了避免和原生的自定义内置元素相混淆


三 .全局注册：
---
> 我们可以使用 Vue 应用实例的 app.component() 方法，让组件在当前 Vue 应用中全局可用
```js
import { createApp } from 'vue'

const app = createApp({})

app.component(
  // 注册的名字
  'MyComponent',
  // 组件的实现
  {
    /* ... */
  }
)
```
> 如果使用单文件组件，你可以注册被导入的 .vue 文件：
```js
import MyComponent from './App.vue'

app.component('MyComponent', MyComponent)
app.component() 方法可以被链式调用：

js
app
  .component('ComponentA', ComponentA)
  .component('ComponentB', ComponentB)
  .component('ComponentC', ComponentC)
```
> 全局注册的组件可以在此应用的任意组件的模板中使用：
```html
<!-- 这在当前应用的任意组件中都可用 -->
<ComponentA/>
<ComponentB/>
<ComponentC/>
```
> 所有的子组件也可以使用全局注册的组件，这意味着这三个组件也都可以在彼此内部使用

四 .局部注册:
---
+ 全局注册虽然很方便，但有以下几个问题：

    1. 全局注册，但并没有被使用的组件无法在生产打包时被自动移除 (也叫“tree-shaking”)。如果你全局注册了一个组件，即使它并没有被实际使用，它仍然会出现在打包后的 JS 文件中
    2. 全局注册在大型项目中使项目的依赖关系变得不那么明确。在父组件中使用子组件时，不太容易定位子组件的实现。和使用过多的全局变量一样，这可能会影响应用长期的可维护性

    > 相比之下，局部注册的组件需要在使用它的父组件中显式导入，并且只能在该父组件中使用。它的优点是使组件之间的依赖关系更加明确，并且对 tree-shaking 更加友好

> 局部注册需要使用 components 选项：
```js
<script>
import ComponentA from './ComponentA.vue'

export default {
  components: {
    ComponentA
  }
}
</script>

<template>
  <ComponentA />
</template>
```
> 对于每个 components 对象里的属性，它们的 key 名就是注册的组件名，而值就是相应组件的实现。上面的例子中使用的是 ES2015 的缩写语法，等价于：
```js
export default {
  components: {
    ComponentA: ComponentA
  }
  // ...
}
```
##### 请注意：局部注册的组件在后代组件中并不可用。在这个例子中，ComponentA 注册后仅在当前组件可用，而在任何的子组件或更深层的子组件中都不可用
五 .组件名格式：
---
+ 在整个指引中，我们都使用 PascalCase 作为组件名的注册格式，这是因为：

    1. PascalCase 是合法的 JavaScript 标识符。这使得在 JavaScript 中导入和注册组件都很容易，同时 IDE 也能提供较好的自动补全

    2. `<PascalCase />` 在模板中更明显地表明了这是一个 Vue 组件，而不是原生 HTML 元素。同时也能够将 Vue 组件和自定义元素 (web components) 区分开来


+ 为了方便，Vue 支持将模板中使用 kebab-case 的标签解析为使用 PascalCase 注册的组件。这意味着一个以 MyComponent 为名注册的组件，在模板中可以通过 `<MyComponent>` 或 `<my-component>` 引用。这让我们能够使用同样的 JavaScript 组件注册代码来配合不同来源的模板