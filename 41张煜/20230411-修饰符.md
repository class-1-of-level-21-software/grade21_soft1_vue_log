# 表单绑定修饰符和生命周期钩子

## 一.表单绑定修饰符

### .lazy 

+ 默认情况下，v-model 会在每次 input 事件后实时同步更新数据 (IME 拼字阶段的状态例外也就是输入法打字时例外)

+ 加 lazy 修饰符来改为在每次 change 事件后更新数据，也就是焦点不在对于的事件上时更新，不实时同步更新

```html
<!-- js -->
<script>
export default{
    data(){
        return{
            mod:'',
            mod1:'',
        }
    },
}
</script>

<!-- html -->
<template>
    <div>
        <!-- v-model 会在每次 input 事件后更新数据 (IME 拼字阶段的状态例外（输入法使用时）)。 -->
        <input type="text" v-model="mod">
        {{ mod }}
    </div>
    <div>
        <!-- 添加修饰符 lazy:必须焦点离开，数据才会更新 -->
        <input type="text" v-model.lazy="mod1">
        {{ mod1 }}
    </div>
</template>
```

### .reim

+ 默认自动去除用户输入内容的前后两端的空格
```html
<!-- js -->
<script>
export default{
    data(){
        return{
            arr:''
        }
    },
}
</script>

<!-- html -->
<template>

    <div>
        <!-- 默认自动去除用户输入内容中前后两端的空格 -->
        <input type="text" v-model.trim="arr">
        {{ arr }}
    </div>
</template>
```

#
## 二. 生命周期钩子
```
每个 Vue 组件实例在创建时都需要经历一系列的初始化步骤，在此过程中，它也会运行被称为生命周期钩子的函数，让开发者有机会在特定阶段运行自己的代码。
```

### mounted ：在选项式中
+ mounted 钩子可以用来在创建的所有 DOM 节点和组件完成初始渲染后运行代码

+ 周期钩子最常用的是 **mounted**、updated 和 unmounted。

+ 有生命周期钩子函数的 this 上下文都会自动指向当前调用它的组件实例。注意：避免用箭头函数来定义生命周期钩子，因为如果这样的话你将无法在函数中通过 this 获取组件实例。

```html
<!-- js -->
<script>
export default{
    data(){
        return{

        }
    },
    mounted(){
        console.log('渲染成功')
       setTimeout(() => {
        console.log('限时显示渲染成功')
       }, 2000);
    }
}
</script>

<!-- html -->
<template>
    <p>1111</p>
</template>
```

### onMounted ：在组合式中

+ 周期钩子最常用的是 **onMounted**、onUpdated 和 onUnmounted。

+ 对 onMounted 的调用**不一定**要放在 setup() 或 <script setup> 内的词法上下文中

+ onMounted() 也可以在一个外部函数中调用，只要调用栈是同步的，且最终起源自 setup() 就可以。

```html
<!-- 组合式 -->
<script setup>

import {onMounted} from 'vue'

onMounted(()=>{
    console.log('组件和DOM节点全部渲染成功')

    setTimeout(() => {
        console.log('延时成功')
    }, 2000);
})
</script>

<template>
    <p>敲代码了</p>
</template>
```