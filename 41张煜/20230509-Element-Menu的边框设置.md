# Menu 菜单

* 为网站提供导航功能的菜单。

1. 顶栏
2. 左右
3. 侧栏
4. 折叠面板

# Menu Attributes

* 属性名：collapse  说明：是否水平折叠收起菜单（仅在 mode 为 vertical 时可用）  类型：boolean   默认值：false
* 属性名：text-color    说明：字体颜色
* 属性名:active-text-color  说明：选中菜单栏时字体颜色
* 属性名：index 说明：唯一标志	
* 插槽名：title 说明：自定义标题内容
* 事件名：click 说明：点击菜单时的回调函数

# Menu的border-size
```
.el-menu{
    border-size:solid 1px '选择合适的颜色'
}
```
