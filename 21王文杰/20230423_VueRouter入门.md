## 安装

```
npm install vue-router 或 yarn add vue-router
```

## router-link
请注意，我们没有使用常规的 a 标签，

而是使用一个自定义组件 router-link 来创建链接。

这使得 Vue Router 可以在不重新加载页面的情况下更改 URL，

处理 URL 的生成以及编码。

## router-view

router-view 将显示与 url 对应的组件。

你可以把它放在任何地方，以适应你的布局。

## 基本使用
main.js:
```js

import { createApp } from 'vue'
import App from './App.vue'
// 定义若干个组件并引入
import my from './components/My.vue'
import he from './components/He.vue'
import you from './components/You.vue'
// 安装vue-router 并引入需要的方法
import { createRouter, createWebHistory } from 'vue-router'

// 定义一个对象数组，里面放路由路径，和连接的组件
const routes = [
    { path: '/', component: my },
    { path: '/he', component: he },
    { path: '/you', component: you }
]

// 使用方法创建路由，history里放createWebHistory()，这会显示在路径上，定义的对象数组放上去
const router = createRouter(
    {
        history: createWebHistory(),
        routes
    }
)

// 创建app并使用刚刚创建的路由
createApp(App).use(router).mount('#app')
```

My.vue：

router-link是入口，最终会渲染成a链接

```html
<template>
    <router-link to="/you">111</router-link>
    <router-link to="/he">222</router-link>
</template>
```

He.vue：

```html
<template>
     <router-link to="/">111</router-link>
</template>
```

You.vue：

```html
<template>
    <router-link to="/">12222</router-link>
</template>
```
APP.vue
APP.vue是出口，只需要放一个router-view，拥有显示三个组件的内容
```html
<template>
  <router-view></router-view>
</template>


```
