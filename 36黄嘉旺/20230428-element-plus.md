# Element-plus

## 用法
#### 完整引入
1. 如果你对打包后的文件大小不是很在乎，那么使用完整导入会更方便 

```vue
// main.ts
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')

```