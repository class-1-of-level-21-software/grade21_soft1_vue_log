#  20230331 =》 No.02-Vue-梳理vue的各种指令 -Directives- 

## I.分别为完整用法以及缩写用法分布上下

### 1. v-bind


```

<div v-bind:id="dynamicId"></div>

```

------

```

<div :id="dynamicId"></div>

```
------

### 2. v-on

```

<a v-on:click="doSomething"> ... </a>


```

------

```

<a @click="doSomething"> ... </a>

```

------

## II.分清楚 "v-if" or ":disabled"

```
当我们需要频繁切换一事物存在或出现时则使用 "v-if",反之使用"以达到禁用的功能"
```


------

![show](https://cn.vuejs.org/assets/directive.69c37117.png)

☆￣(＞ ☆)时光不老     ☆￣(＞ ☆)我们不散
------



------




<font face="STCAIYUN" size=7>__.log__2023.0331</font>
------
