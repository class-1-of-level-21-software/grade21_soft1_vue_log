# 简单地概括一下今日所学！



## 一.列表渲染！！

### 1.首当其冲，第一个是我们常见的 '倒序' 也就是 reserve

![show](./IMG/0407IMG/%E4%BD%BF%E7%94%A8%E5%BB%B6%E6%97%B6%E5%87%BD%E6%95%B0%26reverse%E6%96%B9%E6%B3%95%E5%B0%86%E6%95%B0%E7%BB%840-9%E5%AE%9E%E7%8E%B0%E5%BB%B6%E6%97%B6%E7%BF%BB%E8%BD%AC.png)

------

### 2.其二呢！拿出来了第二大镇山之宝，也就是 普普通通 过滤器 ———— 'filter'

![show](./IMG/0407IMG/%E5%9C%A8%E8%A1%8C%E5%86%85%E4%BD%BF%E7%94%A8filter%E8%BF%87%E6%BB%A4%E5%99%A8%E5%B0%86%E6%95%B0%E7%BB%84%E5%86%85%E5%AE%B9%E5%8F%AA%E6%98%BE%E7%A4%BA%E5%81%B6%E6%95%B0%E5%BD%A2%E5%BC%8F.png)


------

##### ps:没想到吧，还有另外一种等效的方式

![show](./IMG/0407IMG/%E5%B0%86%E4%BD%BF%E7%94%A8filter%E8%BF%87%E6%BB%A4%E5%99%A8%E5%B0%86%E6%95%B0%E7%BB%84%E5%86%85%E5%AE%B9%E4%BB%A5%E5%81%B6%E6%95%B0%E7%9A%84%E5%BD%A2%E5%BC%8F%E5%B1%95%E7%A4%BA%E7%9A%84%E6%96%B9%E6%B3%95%E4%BB%8E%E8%A1%8C%E5%86%85%E6%94%B9%E5%86%99%E4%B8%BA%E4%BD%BF%E7%94%A8%E5%91%A8%E6%9C%9F%E6%96%B9%E6%B3%95%E5%B0%81%E8%A3%85.png)

------

## 二.事件处理

## 1.事件处理器

### I.内联事件处理器：事件被触发时执行的内联 JavaScript 语句 (与 onclick 类似)

例如：
------
    
```js
<script>
    export default{
        data() {
            return {
                 count: 0
            }
        }

    }
</script>

<template>
<button @click="count++">点我自增！！！</button>
<p>按钮已经被点击了{{ count }}次</p>
</template>
```

![show](./IMG/0407IMG/%E8%BF%99%E9%87%8C%E6%98%AF%E5%86%85%E8%81%94%E4%BA%8B%E4%BB%B6%E5%A4%84%E7%90%86%E5%99%A8%E7%A4%BA%E4%BE%8B%E5%9B%BE.png)



------


### II.方法事件处理器：一个指向组件上定义的方法的属性名或是路径

ps：随着事件处理器的逻辑变得愈发复杂，内联代码方式变得不够灵活。因此 v-on 也可以接受一个方法名或对某个方法的调用

```js
<script>
export default {
  data() {
    return {
      name: '小猪猪，欢迎回家！已为您打开室内空调以及地暖，请问是否启动居家'代码开发模式'，立即进入工作状态！'
    }
  },
  methods: {
    greet(event) {
      // `this` inside methods points to the current active instance 方法中的 `this` 指向当前活跃的组件实例
      alert(`Hello ${this.name}!`)
      // `event` is the native DOM event `event` 是 DOM 原生事件
      if (event) {
        alert(event.target.tagName)
      }
    }
  }
}
</script>



<template>
    <!-- `greet` 是上面定义过的方法名(也就是欢迎的意思！) -->
	<button @click="greet">Greet</button>
</template>

```


![show](./IMG/0407IMG/%E8%BF%99%E9%87%8C%E6%98%AF%E6%96%B9%E6%B3%95%E4%BA%8B%E4%BB%B6%E5%A4%84%E7%90%86%E5%99%A8%E7%A4%BA%E4%BE%8B%E5%9B%BE.png)




------


![show](./77代往生堂堂主大人/十七个客户.jpg)



------

<font face="STCAIYUN" size=7>__.log__2023.0407</font>
------
