# 嵌套路由:children

详细的官方网站 : https://router.vuejs.org/zh/guide/essentials/nested-routes.html

#
## 使用方式

+ 在自己的路由配置文件里面创建对应的路由

+ 子路由需要在 父路由 对应的参数中使用 **children** 配置；

+ 切记，在children 中，子路由的路径不要加 / ；
```js

//定义路由
const routes=[
    {path:'/',component:Home},
    {
        path:'/about',
        component:About,
        //嵌套路由
        children:[
            {
                //当 /about/:id 是匹配成功
                //(\\d+)自能为数字
                path:':id(\\d+)',
                // AboutId 将被渲染到 About 的 RouterView 内部
                component:AboutId
            }
        ]
    },
    {path:'/student',component:Student},
    {path:'/student/:id(\\d+)',component:StudentId},
    {path:'/:pathMatch*',component:NotFund}
]
```