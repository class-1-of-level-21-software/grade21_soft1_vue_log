# 导航栏的封装

## 标签

+ 最外面一层是由 <**el-menu**> <**/el-menu**> 标签 包裹

+ 一级导航栏: 用 <**el-menu-item**> <**/el-menu-item**> 标签 包裹

+ 一级导航栏下的子导航栏:用 <**el-sub-menu**><**/el-sub-menu**> 标签包裹

+ <**el-menu-item-group**> 组件可以实现菜单进行分组，分组名可以通过title属性直接设定，也可以通过具名 slot 来设定

+ 根据 index 可以限制收起和展开

#
## 代码

+ Nav.vue(封装的文件)
```vue
<template>
    <div>
        <el-menu>
            <template v-for="menu of menus">
                <!-- 判断是否有嵌套 -->
                <!-- 
                    @open：展开对应的 index 
                    @close: 关闭对应的 index

                    有对应的index 也可以控制展开和关闭
                 -->
                <el-sub-menu 
                    v-if="menu.children && menu.children.length>0"
                    :index="i++"
                >
                    <template #title>
                        <span>{{ menu.title }}</span>
                    </template>
                    <!-- 因为里面有嵌套，所以继续执行该组件 -->
                    <Navs :menus="menu.children"></Navs>
                </el-sub-menu>

                <!-- 没有嵌套执行 -->
                <el-menu-item v-else>
                    {{ menu.title }}
                </el-menu-item>
            </template>
        </el-menu>
    </div>
</template>

<script setup>
import {ref} from 'vue'

// 设置一个判断收起和展开的变量
const i=ref(0)

// 将变量抛出
const props=defineProps(['menus'])
</script>
```
+ Home.vue
```vue
<template>
    <div>
        <el-container>
            <!-- 侧边栏 -->
            <el-aside>
                <!-- <Nav></Nav> -->
                <Navs :menus="menus"></Navs>
            </el-aside>

            <!-- 容器 -->
            <el-container>
                <!-- 顶栏 -->
                <el-header class="el-header"></el-header>

                <!-- 主内容 -->
                <el-main></el-main>

                <!-- 底栏 -->
                <el-footer class="el-footer"></el-footer>
            </el-container>
        </el-container>
    </div>
</template>
<script setup>
import Nav from './Nav.vue';
import Navs from './Nav/Navs.vue';

const menus = [
    {
        title: '炸类',
        children: [
            {
                title: '炸鸡腿'
            }, {
                title: '炸鸡'
            }
        ]
    }, {
        title: '火锅'
    }, {
        title: '饮料',
        children: [
            {
                title: '可乐'
            }, {
                title: '咖啡',
                children: [
                    {
                        title: '拿铁咖啡'
                    }, {
                        title: '卡布奇若'
                    }
                ]
            }
        ]
    }
]
</script>
<style scoped>
.el-aside{
    background-color: aqua;
    height: calc(100vh);
}
.el-header{
    background-color: blue;
}
.el-footer{
    background-color: yellow;
}
</style>
```
![1](./img/230505/1.png)