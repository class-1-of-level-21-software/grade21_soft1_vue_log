# 组件注册

详细的网站: https://cn.vuejs.org/guide/components/registration.html

#
## 一.全局注册: component() 方法

+ 在vue项目中的main.js文件中使用app.component() 方法

+ app.component() 方法可以被链式调用

```js
// main.js
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

//导入文件
import f1 from './components/f1.vue'
import f2 from './components/f2.vue'

const app=createApp(App)

//注册被导入的文件，链式调用
app.component('f1',f1).component('f2',f2)
app.mount('#app')

```
```vue
<!-- App.vue -->
<script setup>
</script>

<template>
<f1/>
  <h1>测试</h1>
<f2></f2>
</template>

<!-- f1.vue -->
<template>
    <div>
        <h1>吃饭了</h1>
    </div>
</template>
```
![1](./img/230417/1.png)


#
## 二.局部注册

+ 局部注册的组件在后代组件中并不可用。

+ 在选项式中，需要使用**components** 选项注册
```vue
<script>
import ComponentA from './ComponentA.vue'

export default {
  components: {
    ComponentA
  }
}
</script>

<template>
  <ComponentA />
</template>

```

+ 在组合式中，导入的组件可以直接在模板中使用，无需注册
```vue
<script setup>
import ComponentA from './ComponentA.vue'
</script>

<template>
  <ComponentA />
</template>

```

#
## 三.全局和局部对比


### 全局注册的问题:

+ 1. 全局注册，但并没有被使用的组件无法在生产打包时被自动移除 (也叫“tree-shaking”)。如果你全局注册了一个组件，即使它并没有被实际使用，它仍然会出现在打包后的 JS 文件中。

+ 2. 全局注册在大型项目中使项目的依赖关系变得不那么明确。在父组件中使用子组件时，不太容易定位子组件的实现。和使用过多的全局变量一样，这可能会影响应用长期的可维护性。

### 局部

+ 1. 局部注册的组件需要在使用它的父组件中显式导入，并且只能在该父组件中使用。

+ 2. 它的优点是使组件之间的依赖关系更加明确，并且对 tree-shaking 更加友好。