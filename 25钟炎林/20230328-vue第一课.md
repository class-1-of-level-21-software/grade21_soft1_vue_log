# Vue
1. Vue (发音为 /vjuː/，类似 view) 是一款用于构建用户界面的 JavaScript 框架。它基于标准 HTML、CSS 和 JavaScript 构建，并提供了一套声明式的、组件化的编程模型，帮助你高效地开发用户界面。无论是简单还是复杂的界面，Vue 都可以胜任。

```vue
<!-- HelloWord.vue -->
<script setup>


defineProps({
  msg: String,
})

let btnVal='点我看控制台'
let s=0
function count() {
    console.log(s++); 
}

</script>

<template>
  <h1>{{ msg }}</h1>
  <input type="button"  :value="btnVal" @click="count">
</template>

```
```vue
<!-- App.vue -->
<script setup>
import HelloWorld from './components/HelloWorld.vue'
</script>

<template>

  <HelloWorld msg="first project" />
</template>
```