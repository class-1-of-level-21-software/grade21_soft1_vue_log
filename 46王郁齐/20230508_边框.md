```
<template>
  <div class="common-layout">
    <el-container>
      <el-aside width="200px">
        <Navbar :is-collapse="isCollapse" :menus="menuList"></Navbar>
      </el-aside>
      <el-container>
        <el-header>
          <el-icon :class="{ rotateClass: isCollapse }" :size="30" @click="iconClickHandler">
            <fold></fold>
          </el-icon>
        </el-header>
        <el-main>
          <div class="inline-flex" :style="obj">

          </div>
        </el-main>
        <el-footer>Footer</el-footer>
      </el-container>
    </el-container>
  </div>
</template>
<script setup>
import { ref, reactive } from 'vue';
import Navbar from './components/Navbar.vue';

const isCollapse = ref(false)
const menuList = reactive([
  {
    title: '系统管理',
    meta: { icon: 'setting' },
    children: [
      {
        title: '用户管理',
        meta: { icon: 'User' }
      },
      {
        title: '角色管理',
        meta: { icon: 'Grid' }
      },
      {
        title: '权限管理',
        meta: { icon: 'Promotion' }
      },
      {
        title: '部门管理',
        meta: { icon: 'HelpFilled' },
        children: [
          {
            title: '销售部',
            meta: { icon: 'user' }
          },
          {
            title: '研发部',
            meta: { icon: 'user' }
          },
          {
            title: '财务部',
            meta: { icon: 'user' },
            children: [
              {
                title: '出纳'
              },
              {
                title: '会计',

              },
              {
                title: '财务总监'
              },
            ]
          },
        ]
      },
    ]
  }, {
    title: '学生管理',
    meta: { icon: 'setting' }
  }
])
const obj = reactive({ boxShadow: 'var("--el-box-shadow-dark")', height: '30px', width: '30px', backgroundColor: 'black' })


const iconClickHandler = function () {
  console.log('888');
  isCollapse.value = !isCollapse.value
}
</script>
<style scoped>
.el-aside {
  background-color: white;
  height: calc(100vh);
  /* flex: 2 2 auto; */
}

.el-header {
  width: 600px;
  background-color: red;
  display: flex;
  align-items: center;
}

.el-main {
  background-color: blue;
}

.el-footer {
  background-color: #A3A6AD;
}

.rotateClass {
  transform: rotate(-90deg);
}
</style>
```