
## Props 声明
```js
//HelloWord.vue
defineProps({
  title: String,
  likes: Number
})
<p>{{title}}</p>
<p>{{likes}}</p>


//App.vue
import HelloWord from './HelloWord.vue'
const titles='666'
const like=123
<HelloWord :title=titles :likes=like/>
```

## Props 细节

相比于推荐的更贴近的HTML风格的书写方式，我还是更喜欢用原来的名字来使用它
```js
//MyComponen.vue
defineProps({
  greetingMessage: String
})
<span>{{ greetingMessage }}</span>

//App.vue
//官方推荐书写风格
<MyComponent greeting-message="hello" />
//自己推荐书写风格
<MyComponent greetingMessage="hello" />
```