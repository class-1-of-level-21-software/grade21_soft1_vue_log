# 路由数据转换成菜单栏数据

## 一.步骤

+ 1、将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径
    
    + a. 将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径

    + b. 定义一个空数组,用于存储处理好的路由

    + c. 循环遍历传进的数组，处理每一个路由

        + (1). 定义一个对象，存储需要的数据

        + (2). 判断传传入的上级节点的路径path是否为空同时原来的路径是否为空，进行相关的处理

        + (3). 判断是否有下级节点，递归调用converPath 函数,进行相关处理

        + (4). 将处理好的路由插入到空的数组中
    
    + d. 返回定义的存储变量

+ 2.排除一下主页面的路由，使其不会出现在菜单栏(请看下一个笔记)

+ 3.返回处理好的菜单栏数据

```js
// 使用计算属性，将导入的路由，转换成菜单栏数据
const menus = computed(() => {
    //1、将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径
    let list = converPath(routes, '')

    // 2.排除一下主页面的路由，使其不会出现在菜单栏
    let menuList=processHidden(list)

    // 3.返回处理好的菜单栏数据
    return menuList
})
```

#
## 二.代码
```js
//将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径
// 暴露方法
export function converPath(arr, parentPath) {
    //定义一个空数组
    let list = [];

    // 处理每一个路由
    arr.forEach(item=>{

        // 定义一个对象
        let tmp={
            path:item.path,
            men:item.men?item.men:{title:'标题',icon:'setting'}
        }

        // 当传入的上级节点的路径path不为空同时原来的路径也不为空时，就叠加
        if(parentPath && tmp.path){
            if(parentPath==='/'){
                tmp.path=parentPath+tmp.path
            }else{
                tmp.path=parentPath+'/'+tmp.path
            }
        }

        // 判断是否有下级节点，递归调用converPath 函数
        if (item.children && item.children.length > 0) {
            // 传入下级路由（是个数组），并且传入当前路由的路径
            let children=converPath(item.children,item.path)

            if(children.length>0){
                tmp.children=children
            }
        }

        //将处理好的路由插入到空的数组中
        list.push(tmp)
    })
    
    return list
}
```