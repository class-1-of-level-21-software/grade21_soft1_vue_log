## 计算属性
### 1. 基础示例
```
<script>
import { computed } from 'vue';

export default {
  data(){
    return {
      ddr:['1']
    }
  },
  computed:{
    ishot() {
      return this.ddr.length > 0 ? '有' : '没有'
    }
  }
}
</script>


<template>
  <div>你{{ ishot }}东西</div>
</template>
```
示例：

[![pp4b0Zq.jpg](https://s1.ax1x.com/2023/04/04/pp4b0Zq.jpg)](https://imgse.com/i/pp4b0Zq)
### 2.计算属性缓存 vs 方法​
```
你可能注意到我们在表达式中像这样调用一个函数也会获得和计算属性相同的结果：

template
<p>{{ calculateBooksMessage() }}</p>
js
// 组件中
methods: {
  calculateBooksMessage() {
    return this.author.books.length > 0 ? 'Yes' : 'No'
  }
}
若我们将同样的函数定义为一个方法而不是计算属性，两种方式在结果上确实是完全相同的，然而，不同之处在于计算属性值会基于其响应式依赖被缓存。一个计算属性仅会在其响应式依赖更新时才重新计算。这意味着只要 author.books 不改变，无论多少次访问 publishedBooksMessage 都会立即返回先前的计算结果，而不用重复执行 getter 函数。

这也解释了为什么下面的计算属性永远不会更新，因为 Date.now() 并不是一个响应式依赖：

js
computed: {
  now() {
    return Date.now()
  }
}
相比之下，方法调用总是会在重渲染发生时再次执行函数。

为什么需要缓存呢？想象一下我们有一个非常耗性能的计算属性 list，需要循环一个巨大的数组并做许多计算逻辑，并且可能也有其他计算属性依赖于 list。没有缓存的话，我们会重复执行非常多次 list 的 getter，然而这实际上没有必要！如果你确定不需要缓存，那么也可以使用方法调用。
```

## 类与样式绑定
### 1.基本示例
```
import { computed } from 'vue';

export default {
  data(){
    return {
      dd:true,
    }
  }
}
</script>

<template>
  <p :class="{jx:dd}"> cjxdw </p>
</template>

<style>
.jx {
  color: red;
  background: black;
  font-size: 50px;
}
</style>

```
示例：

[![pp4qBXd.jpg](https://s1.ax1x.com/2023/04/04/pp4qBXd.jpg)](https://imgse.com/i/pp4qBXd)


## class绑定对象：
```
<script>
export default {
  data() {
    return {
       cjx: {
        cc:true,
        xx:false,
        jj:true
       }
    }
  }
}
</script>

<template>
<p :class="cjx">111</p>
</template>
```
## 效果图：
[![ppIWbVS.jpg](https://s1.ax1x.com/2023/04/06/ppIWbVS.jpg)](https://imgse.com/i/ppIWbVS)


## class绑定数组：
```
<script>
export default {
  data() {
    return {
       cjxdw: [
        {cc1:true},
        {xx1:true},
        {jj2:false}
       ]
    }
  }
}
</script>

<template>
<p :class="cjxdw">111</p>
</template>
```
## 效果图：
[![ppIhiOP.jpg](https://s1.ax1x.com/2023/04/06/ppIhiOP.jpg)](https://imgse.com/i/ppIhiOP)