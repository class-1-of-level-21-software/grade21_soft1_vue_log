# element-ui
## 网址
```
https://element-plus.gitee.io/zh-CN/
```
## 安装
```
yarn add element-plus
```
## 引入 Element
你可以引入整个 Element，或是根据需要仅引入部分组件。我们先介绍如何引入完整的 Element。

## 完整引入
在 main.js 中写入以下内容：
```
import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'

const app = createApp(App)

app.use(ElementPlus)
app.mount('#app')
```
以上代码便完成了 Element 的引入。需要注意的是，样式文件需要单独引入。
