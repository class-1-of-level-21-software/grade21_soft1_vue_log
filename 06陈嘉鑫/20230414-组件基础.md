# 组件基础
## 自定义组件
```
在父组件用import导入,用components注册就可以使用啦
```
[![ppz7qfO.md.jpg](https://s1.ax1x.com/2023/04/14/ppz7qfO.md.jpg)](https://imgse.com/i/ppz7qfO)
## 父传子
### 用props接收传过来的值
[![ppzbMqA.jpg](https://s1.ax1x.com/2023/04/14/ppzbMqA.jpg)](https://imgse.com/i/ppzbMqA)
### 在组件上绑定要传的值
[![ppzbYRS.jpg](https://s1.ax1x.com/2023/04/14/ppzbYRS.jpg)](https://imgse.com/i/ppzbYRS)
### 效果图
[![ppzbUMQ.jpg](https://s1.ax1x.com/2023/04/14/ppzbUMQ.jpg)](https://imgse.com/i/ppzbUMQ)
## 子传父
### 用emit触发
[![ppzjcDg.jpg](https://s1.ax1x.com/2023/04/14/ppzjcDg.jpg)](https://imgse.com/i/ppzjcDg)
### 用@方法监听
[![ppzjREj.md.jpg](https://s1.ax1x.com/2023/04/14/ppzjREj.md.jpg)](https://imgse.com/i/ppzjREj)
### 效果图
[![ppzjz26.md.jpg](https://s1.ax1x.com/2023/04/14/ppzjz26.md.jpg)](https://imgse.com/i/ppzjz26)
