# 组件基础

详细的介绍网站: https://cn.vuejs.org/guide/essentials/component-basics.html

#
## 一.组件的使用 :两种是因为API的不同，所有写法不同

+ 组合式(组合式的API)
```html
<!-- 组合式 -->
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import DefineProps from './components/DefineProps.vue';
</script>

<template>
  <HelloWorld />
  <DefineProps/>
</template>
```

+ 选项式(选项式式的API):

    + 要将导入的组件暴露给模板，我们需要在 components 选项上注册它。

    + 这个组件将会以其注册时的名字作为模板中的标签名。

```html
<!-- 选项式 -->
<script>
//导入子组件，但是要注册，才能将导入的组件暴露给模板
import HelloWorld from './components/HelloWorld.vue';
import DefineProps from './components/DefineProps.vue';

export default{
  //注册导入的子组件
  components:{
    HelloWorld,DefineProps
  },
}
</script>

<template>
  <HelloWorld />
  <DefineProps/>
</template>
```

### 传递 props

+ Props 是一种特别的 attributes，你可以在组件上声明注册。

+ defineProps :组合式

    + HelloWorld.vue
    ```html
    <template>
        <h1>{{ xx }}</h1>
        <h3 v-if="id">第 {{ id }} 个选择: {{ name }}</h3>
    </template>

    <script setup>

    defineProps(['xx','id','name'])
    </script>
    ```

    + App.vue
    ```html
    <!-- 组合式 -->
    <script setup>
    import HelloWorld from './components/HelloWorld.vue'
    import DefineProps from './components/DefineProps.vue';

    const add=[
    {id:'1',name:'炸鸡'},
    {id:'2',name:'猪脚饭'},
    {id:'3',name:'炒饭'},
    {id:'4',name:'炒面'},
    ]
    </script>
    <template>
    <HelloWorld xx="吃饭了，饿了"/>
    <HelloWorld v-for="item of add" :key="item.id" :id="item.id" :name="item.name"/>
    </template>
    ```
  

+ props: 选项式:
    
    +  当一个值被传递给 prop 时，它将成为该组件实例上的一个属性。该属性的值可以像其他组件属性一样，在模板和组件的 this 上下文中访问。

    + 一个组件可以有任意多的 props，默认情况下，所有 prop 都接受任意类型的值。

+ 代码

    + DefineProps.vue
    ```html
    <template>
    <h1>{{ xx }}</h1>
    
    <!-- 当id为空时，这个标签内容不显示 -->
    <h3 v-if="id">第{{id}}选择:{{ name }}</h3>
    </template>

    <script>
    export default{
    data(){
        return{

        }
    },
    props:['xx','name','id']
    }
    </script>
    ```

    + App.vue
    ```html
    <!-- 组合式 -->
    <script setup>
    import HelloWorld from './components/HelloWorld.vue'
    import DefineProps from './components/DefineProps.vue';

    const add=[
    {id:'1',name:'炸鸡'},
    {id:'2',name:'猪脚饭'},
    {id:'3',name:'炒饭'},
    {id:'4',name:'炒面'},
    ]
    </script>

    <template>
    <DefineProps xx="点饭了，点什么?"/>
    <DefineProps v-for="item of add" :key="item.id" :id="item.id" :name="item.name"/>
    </template>
    ```

#
## 二.监听事件

+ 调用内置的 $emit 方法，通过传入事件名称来抛出一个事件

```html
<template>
 <h1>{{name}}</h1>
 <input type="button" value="变大" @click="$emit('bianda')" >
 <input type="button" value="变小" @click="$emit('xiao')" >
</template>

```

### 选项式

+ 通过 emits 选项来声明需要抛出的事件

```html
<!-- HelloWorld -->
<script>
export default {
  props: ['name'],
  emits: ['bianda' ,'xiao']
}
</script>
<template>
 <h1>{{name}}</h1>
 <input type="button" value="变大" @click="$emit('bianda')" >
 <input type="button" value="变小" @click="$emit('xiao')" >
</template>

<!-- App.vue -->

<script>
import HelloWorld from './components/HelloWorld.vue'

export default{
  components:{
    HelloWorld
  },
  data(){
      return{
        x:1
      }
  },
  methods:{
    bianda(){
        this.x+=1;
    },
    xiao(){
      this.x-=1;
    }
  }
}
</script>

<template>
  <div :style="{fontSize: x + 'em'}">
    <HelloWorld @bianda="bianda" name="睡觉" @bianxiao="xiao"/>
  </div>
</template>
```

### 组合式

+ 通过 defineEmits 宏来声明需要抛出的事件

```html
<!--  HelloWorld -->
<script setup>
  defineProps(['name'])
  defineEmits(['bianda'])
</script>

<template>
 <h1>{{name}}</h1>
 <input type="button" value="变大" @click="$emit('bianda')" >
 <input type="button" value="变小" @click="$emit('xiao')" >
</template>


<!-- App.vue -->

<!-- 组合式 -->
<script setup>

import HelloWorld from './components/HelloWorld.vue';

import {ref} from 'vue'

const x=ref(1)
console.log(x.value)

function bianda(){
  x.value+=1;
}

function xiao(){
  x.value-=1;
}

</script>


<template>
  <div :style="{fontSize: x + 'em'}">
    <HelloWorld @bianda="bianda" name="睡觉" @xiao="xiao"/>
  </div>
</template>
```



## 三.<slot> 元素 :通过插槽输入内容

+ <slot> 元素来实现向组件传递内容

+ 使用 <slot> 作为一个占位符，父组件传递进来的内容就会渲染在这里

```html
<!-- Slot.vue -->
<template>
    <div>
        <h1>
            <slot/>
        </h1>
    </div>
</template>

<!-- App.vue -->
<script>
import HelloWorld from './components/HelloWorld.vue'
import Slot from './components/Slot.vue'

export default{
  components:{
    HelloWorld,Slot
  },
}
</script>

<template>
  <Slot>鸡腿</Slot>
</template>
```
