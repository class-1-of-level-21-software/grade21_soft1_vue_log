# Vue入门

## 一.什么是Vue

+ Vue (发音为 /vjuː/，类似 view) 是一款用于构建用户界面的 JavaScript 框架。它基于标准 HTML、CSS 和 JavaScript 构建，并提供了一套声明式的、组件化的编程模型，帮助你高效地开发用户界面。

+ Vue 是一个框架，也是一个生态。其功能覆盖了大部分前端开发常见的需求。

+ Web 世界是十分多样化的，不同的开发者在 Web 上构建的东西可能在形式和规模上会有很大的不同。考虑到这一点，Vue 的设计非常注重灵活性和“可以被逐步集成”这个特点。

### Vue的不同使用方式:

+ 无需构建步骤，渐进式增强静态的 HTML

+ 在任何页面中作为 Web Components 嵌入

+ 单页应用 (SPA)

+ 全栈 / 服务端渲染 (SSR)

+ Jamstack / 静态站点生成 (SSG)

+ 开发桌面端、移动端、WebGL，甚至是命令行终端中的界面


## 二.下载Vue

### 第一种:使用 npm
```js
    npm init vue
```
+ 回车完是想设的文件夹名(可以直接回车)，后面全部直接回车就好了(全是NO)

### 第二种 :使用 yarn
```js
    yarn create vite --template vue

    或者

    yarn create vue

//回车完是想设的文件夹名(可以直接回车)，后面全部直接回车就好了(全是NO)

    或者

    yarn create vite
//要选择
```



## 三.使用Vue

### 更新 npm和yarn
```js
    npm i -g npm//更新npm

    npm i -g yarn //更新yarn
```

### 拉取依赖包
```js
    yarn//不能使用npm拉取
```

### 运行Vue
```js
    yarn dev //不能使用npm运行
```