# 需要api key # 需要api key # 需要api key

```vue

<script>
	const { createApp } = Vue
	createApp({
		data() {
			return {
				api: '',
				msgLoad: false,
				anData: {},
				sentext: '先输入api再发送问题',
				usermsg: '',

				animationData: {},
				showTow: false,
				msgList: [{
					my: false,
					msg: "你好我是chatGPT,请问有什么问题可以帮助您?"
				}],
				msgContent: "",
				msg: "",
				sendmsgcache: ['AI:你好我是chatGPT,请问有什么问题可以帮助您?\n']
			}
		},
		methods: {
			scroend() {
				// 让滚动条始终在最底部
				this.$nextTick(() => {
					this.$refs.messageContent.scrollTop = this.$refs.messageContent.scrollHeight
				})

			},
			entapi() {
				console.log('11');
				this.sentext = 'api检测中'
				axios.post('https://api.openai.com/v1/completions', {
					prompt: '你好呀', max_tokens: 3072, model: "text-davinci-003", temperature: 0
				}, {
					headers: { 'content-type': 'application/json', 'Authorization': 'Bearer ' + this.api }
				}).then(res => {
					console.log('suss', res);
					this.msgLoad = false
					this.sentext = '发送'
				}).catch(error => {
					console.log('error', error);
					console.log(error.code);
					if (error.code == 'ERR_BAD_REQUEST') {
						this.sentext = 'apikey错误，检查确认后重新输入'
						this.msgLoad = true
						return 0;
					}
				})
			},
			sendMsg() {
				// 消息为空不做任何操作
				if (this.msg == "") {
					return 0;
				}
				if (this.msgLoad) {
					return 0;
				}

				if (this.api == "") {
					this.sentext = '请输入apikey再进行请求'
					return 0;
				}
				this.sendmsgcache.push('YOU:' + this.msg + "\n")
				console.log(this.sendmsgcache)
				this.sentext = '请求中'


				this.msgList.push({
					"msg": this.msg,
					"my": true
				})


				this.msgContent = ''

				this.sendmsgcache.forEach(info => {
					console.log('info', info)
					this.msgContent += info

				});

				this.msgLoad = true
				console.log(this.msgContent.length)
				// 清除消息
				this.msg = ""
				this.scroend()

				axios.post('https://api.openai.com/v1/completions', {
					prompt: this.msgContent, max_tokens: 2048, model: "text-davinci-003", temperature: 0
				}, {
					headers: { 'content-type': 'application/json', 'Authorization': 'Bearer ' + this.api }
				}).then(res => {
					console.log(res);
					let text = res.data.choices[0].text.replace("openai:", "").replace("openai：", "").replace(/^\n|\n$/g, "")
					let msglen = res.data.usage.total_tokens
					let msgcomplen = res.data.usage.completion_tokens
					if (msglen > 1500) {
						for (let msg in this.sendmsgcache) {
							this.sendmsgcache.shift()
							if (this.msgContent.length * 1.6 + msglen < 600) {
								console.log('>600')
								break;
							}
						}


					}

					console.log(text);
					this.sendmsgcache.push(text + "\n")
					this.msgList.push({
						"msg": text,
						"my": false
					})

					this.msgLoad = false
					this.sentext = '发送'
					this.scroend()
				}).catch(error => {
					console.log('error', error);
					console.log(error.code);
					this.sentext = '请求失败,重新输入问题发送'
					this.msgLoad = false
				})

			},


		}
	}).mount('#app')
</script>

```

![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230403/uumepNBJwURW.gif)