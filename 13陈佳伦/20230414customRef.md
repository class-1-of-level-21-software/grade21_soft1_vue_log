# customRef 自定义ref

    创建一个自定义的 ref，并对其依赖项跟踪和更新触发进行显式控制。

    如果要使用，记得是在自定义的 ref 中返回一个 customRef，而 customRef 也要返回一个对象

    !实现数据的防抖(数据防抖（debounce）指的是在一段时间内，如果连续多次触发同一事件，只执行最后一次事件，而忽略之前触发的事件。它可以避免一些额外的、不必要的、重复的操作，提升用户体验和系统性能。)

    customRef只能实现浅层次的数据更改
### 基本写法
```vue

<script>
import {customRef} from 'vue'
export default {
  setup () {
    
    const mydata=(value)=>{
      return customRef((track,trigger)=>{
        return {
            get(){
              track();//收集依赖
              return value
            },
            set(newvalue){
              console.log(newvalue);
              trigger() //触发相关依赖
              value=newvalue
            }
        }
      })
    }
    let result = mydata('Hello world')
    console.log('costomref中的数据为',result);
    
    return {
      result,
      
    }
  }
}
</script>
```

### 案例演示

    通过一个小的功能来体现customRef的用法 在页面输入框中输入文字 1秒后下面h1标签显示最新的数据 类似于防抖的功能

```vue
    <template>
  <div>
    <input type="text" v-model="result">
    <h3>{{result}}</h3>
  </div>
</template>

<script>
import {customRef} from 'vue'
export default {
  setup () {
    
    const mydata=(value,delay=1000)=>{
      let timer;
      return customRef((track,trigger)=>{
        return {
            get(){
              track();//收集依赖
              return value
            },
            set(newvalue){
              clearTimeout(timer)
              timer=setTimeout(()=>{
                console.log(newvalue);
                trigger() //触发相关依赖
                value=newvalue
              },delay)
             
            }
        }
      })
    }
    let result = mydata('Hello world')
    console.log('costomref中的数据为',result);
    
    return {
      result, 
    }
  }
}
</script>

```

![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230414/Pdwn0pMADH*t.gif)