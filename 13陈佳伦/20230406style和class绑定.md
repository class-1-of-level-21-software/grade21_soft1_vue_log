# vue style和class的绑定

## style的绑定

1. Vue3中的样式绑定，可以通过:style来绑定行内样式。

### 绑定行内样式

1. 使用只需要将一个对象赋值给:style即可, 该对象的属性名对应css的属性名，属性对应css的属性值。

```vue
    <script>
    export default{
        data(){
            return {
                testcolor:'red',
                bgcolor:'blue',
            }
        }
    }
</script>
<template>
    <div :style="{color:testcolor,'background-color':bgcolor}">
        <p>welcome to chatgpt.mimicheart.top</p>
    </div>
</template>
```
1. 通过:style绑定了CSS color 和 background-color 属性，把它们的值分别赋为textColor和bgColor。这样，该<div>元素就会被渲染成红色字体，黄色背景的样式。


## class的绑定

### 绑定行内样式

1. 使用:class绑定类名也非常简单，只需要把一个数组或对象赋值给:class即可。数组中的元素可以是字符串，也可以是对象；如果是对象，那么该对象的属性名对应CSS类名，属性值为 true 表示要添加这个类，为 false 则表示不添加。

```vue 
 
<template>
  <div :class="classObj"></div>
</template>

<script>
export default {
  data() {
    return {
      classObj: {
        bold: true,
        'bg-gray-200': isActive,
        'text-red-404': !isActive
      },
      isActive: true
    }
  }
}
</script> 

```
