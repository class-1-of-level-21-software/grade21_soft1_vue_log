# attribute绑定

## vue attribute绑定

### v-bind的用法

```vue
<script setup>
    let inputid='i is input' //为input 添加一个id 为 inputid
</script>

<template>
    <input v-bind:id="inputid"> v-bind:id 可以缩写为:id 即 <input :id="inputid">
</template>

```
![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230401/qyo6Iwp4KuFT.png)

### v-bind 动态绑定多个值

```vue
<script setup>
    let obj={
        id:'input',
        class:'inpt',
        type:'text',
        placeholder:'请输入',
    }
</script>

<template>
    <input v-bind="obj">
</template>

```
![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230401/K4H!XveBRcsU.png)

# 事件绑定
## Vue事件绑定指令
 
 ### v-on的用法

 ```vue
<button v-on:click="fn"></button>
 ```
 ### 可以缩写为@:(语法糖形式)(!!分号可不写)
 
```vue
<button @:click="fn"></button>
 ```
 
 ### 函数的调用

```vue
<script setup>
    function fn(event){
        console.log('在调用函数时没有+(),会有一个默认的参数(事件event)');
        console.log(event);
    }
</script>

<template>
    <button v-on:click="fn">单击事件</button>
</template>
```
 
 ![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230401/EST8aS1textD.gif)


```vue
<script setup>
    function fn(event){
        console.log('在调用函数时+(),默认没有参数');
        console.log(event);
    }
</script>

<template>
    <button @:click="fn()">语法糖 双击事件</button>
</template>

```

 ![show](https://gitee.com/Cchen-jialun/picture/raw/master/20230401/SEimBM6B5vPy.gif)