# el-menu的一些设置


## 侧边栏折叠时宽度：

```vue
.el-menu--collapse {
        width: 110px;
      }

```

## 侧边栏展开时宽度：
```vue
.el-menu-vertical-demo:not(.el-menu--collapse) {
        width: 256px;
      }
```

##  菜单选中时设置背景色：
```vue
.el-menu-item.is-active {
        background-color: #1890ff !important;
      }
```

## 菜单hover时设置字体颜色：
```vue
.el-menu-item:hover {
          color: white !important;
        }
```


```
<template>
  <div class="aside">
    <div class="aside-header">
      <img
        src="https://lj-common.oss-cn-chengdu.aliyuncs.com/vue.png"
        style="width: 30px; height: 30px"
      />
      <span
        style="
          font-size: 20px;
          font-weight: 600;
          color: white;
          margin-left: 10px;
        "
        >xxx平台</span
      >
    </div>
    <div class="aside-menu">
      <perfect-scrollbar>
        <el-menu
          :default-active="$route.path"
          class="el-menu-vertical-demo"
          background-color="#191A23"
          text-color="#D1D1D3"
          active-text-color="white"
          unique-opened
        >
          <el-menu-item
            index="/download"
            @click="$router.push({ path: '/download' })"
          >
            <i class="el-icon-location"></i>
            <span slot="title">首页</span>
          </el-menu-item>
          <el-menu-item index="/test" @click="$router.push({ path: '/test' })">
            <i class="el-icon-location"></i>
            <span slot="title">test</span>
          </el-menu-item>
          <el-submenu index="1">
            <template slot="title">
              <i class="el-icon-location"></i>
              <span>导航一</span>
            </template>
            <el-menu-item index="1-3">选项3</el-menu-item>
            <el-submenu index="1-4">
              <template slot="title">选项4</template>
              <el-menu-item index="1-4-1">选项1</el-menu-item>
            </el-submenu>
          </el-submenu>
          <el-submenu index="2">
            <template slot="title">
              <i class="el-icon-location"></i>
              <span>导航2</span>
            </template>
            <el-menu-item index="2-3">选项5</el-menu-item>
          </el-submenu>
        </el-menu>
      </perfect-scrollbar>
    </div>
  </div>
</template>
<script>
export default {
  data() {
    return {};
  },
  methods: {},
  watch: {},
};
</script>
 
 
<style lang='scss' scoped>
.aside {
  height: 100vh;
  .aside-header {
    height: 60px;
    background-color: #191a23;
    line-height: 60px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .aside-menu {
    height: calc(100vh - 60px);
    //perfect-scrollbar默认的类名。自定义滚动条内容区域高度
    .ps {
      height: 100%;
      //展开时宽度
      .el-menu-vertical-demo:not(.el-menu--collapse) {
        width: 256px;
      }
      //折叠时宽度
      // .el-menu--collapse {
      //   width: 110px;
      // }
      .el-menu {
        height: 100%;
        border: 0 !important; //垂直时，去除右侧白边
        .el-menu-item:hover {
          color: white !important;
        }
      }
      .el-menu-item.is-active {
        background-color: #1890ff !important;
      }
    }
  }
}
</style>
```