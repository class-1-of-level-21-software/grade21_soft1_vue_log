```vue
<template>
  <el-menu
    :unique-opened="false"
    :default-active="routerActive"
    class="el-menu-vertical-demo"
    :collapse="store.iscollapse"
    text-color="#ffff"
    router
    @select="handleSelect"
    background-color="transparent"
    :collapse-transition="false"
  >
    <el-menu-item class="logo-menu-item">
      <el-icon v-if="store.iscollapse"><Star /></el-icon>
      <template #title>
        <img class="logo-img" src="@/assets/icons/logo.png" alt="logo" />
      </template>
    </el-menu-item>

    <template v-for="menu in data.menuList" :key="menu.id">
      <el-menu-item
        :index="menu.name + ''"
        v-if="menu.children.length === 0"
        @click="handleClick(menu)"
      >
        <el-icon><i :class="'iconfont ' + menu.icon" aria-hidden="true"></i></el-icon>
        <template #title>{{ menu.title }}</template>
      </el-menu-item>
      <el-sub-menu v-else :index="menu.id + ''">
        <template #title>
          <el-icon><i :class="'iconfont ' + menu.icon" aria-hidden="true"></i></el-icon>
          <span>{{ menu.title }}</span>
        </template>
        <el-menu-item
          @click="handleClick(sub_menu)"
          :index="sub_menu.name"
          v-for="sub_menu in menu.children"
          :key="sub_menu.id"
          >{{ sub_menu.title }}</el-menu-item
        >
      </el-sub-menu>
    </template>
  </el-menu>
</template>

<script>
import { ref, reactive, nextTick, watch, computed } from "vue";
import { useStore } from "@/store/index.js";
import { useRouter,useRoute,onBeforeRouteUpdate} from "vue-router";

export default {
  setup() {
    const store = useStore();
    const router = useRouter();
    const route=useRoute();
    const routerActive = ref("");
    const data = reactive({
      menuList: [
        {
          id: 1,
          icon: "icon-shouye1",
          title: "首页",
          name: "home",
          children: [],
        },
        {
          id: 2,
          icon: "icon-yonghuguanli_huaban",
          title: "用户管理",
          children: [
            {
              id: 3,
              icon:'',
              title: "用户列表",
              name: "userlist",
            },
          ],
        },
        {
          id: 4,
          icon: "icon-tupian-yuanshijituantubiao",
          title: "图片管理",
          children: [
            {
              id: 5,
              icon:'',
              title: "图片列表",
              name: "imagelist",
            },
          ],
        },
        {
          id:6,
          icon:'icon-gerenxinxi',
          title:'个人中心',
          children:[
            {
              id:7,
              icon:'',
              title:"个人信息",
              name:"userinfo"
            }
          ]
        },
        {
          id:7,
          icon:'icon-xitongshezhi',
          title:'系统管理',
          children:[
            {
              id:8,
              icon:'',
              title:"菜单列表",
              name:"menulist"
            }
          ]
        }
      ],
    });
    const handleSelect=(val,path)=>{
      if(path.length>1){
       const test=data.menuList.filter(e=>e.children.find(e=>e.name===val))

      }
    }
    const handleClick = (menu) => {
      store.addTabs(menu);
    };
    const loadRouter = (name) => {
      if(name===undefined){
        name=route.name
      }
      for (let menu of data.menuList) {
        if(menu.name===name){
          routerActive.value=menu.name
          return
        }
        for (let sub_menu of menu.children) {
          if(sub_menu.name===name){
          routerActive.value=sub_menu.name
          return
        }
          
        }
      }
    };
    onBeforeRouteUpdate((to,from,next)=>{
      loadRouter(to.name);
      next()
    })
    loadRouter()
    return {
      store,
      data,
      handleClick,
      routerActive,
      handleSelect
    };
  },
};
</script>

<style lang="scss">
.el-menu {
  border: 0;

  display: flex;
  flex-direction: column;
  background: transparent;
}
.el-menu-vertical-demo:not(.el-menu--collapse) {
  width: 200px;
  min-height: 400px;

}
.el-menu-item:hover {
  background: transparent;
  color: #e5bd7c;
}
.el-sub-menu:hover {
  color: #e5bd7c;
}
.el-sub-menu__title:hover {
  
  color: #e5bd7c;
}
.logo-menu-item {
  height: 70px;
}
.el-menu-item{
  font-size: 10px;
}
.el-sub-menu__title {
  font-size: 10px;

}

.logo-menu-item{
    display: flex;
    justify-content: center;
    align-items: center;
    height: 15vh;
    .logo-img{
      width: 150%;
    }
  
}
</style>




```