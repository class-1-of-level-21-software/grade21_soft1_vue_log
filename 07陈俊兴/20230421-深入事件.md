# 深入事件

详细信息的网站: https://cn.vuejs.org/guide/components/events.html

#
## 一.定义触发的事件

+ 选项式的定义

    + 通过 **emits 选项**来定义它要触发的事件
```vue
<script>
export default {
  emits: ['inFocus', 'submit'],
  methods:{
            fn(){
                this.$emit('someEvent',this.student.name.trim(),this.student.password.trim())
            }
        },
}
</script>

```

+ 组合式的定义

    + 通过 **defineEmits() 宏**来声明它要触发的事件

```vue
<script setup>
const emit = defineEmits(['inFocus', 'submit'])

function buttonClick() {
  emit('submit')
}
</script>

```

#
## 二.事件的监听和校验

+ 通过 **v-on (缩写为 @)** 来监听事件

+ 通过返回值为 `true` 还是为 `false` 来判断验证是否通过

### 组合式
```vue
<!-- 组合式 -->
<script setup>
  import {ref,reactive} from 'vue'

  const student=reactive({
    email:'',
    password:''
  })

        // 定义触发的事件
  const emit=defineEmits({
    //校验传递出来的数据
    someEvent:(email,password)=>{
      if(email.trim()&& password.trim()){
        return true
      }else{
        if(email.trim()){
          console.warn('输入的密码为空，请重新输入')
          return false
        }
        if(password.trim()){
          console.warn('输入的邮箱为空，请重新输入')
          return false
        }
        console.warn('输入的邮箱和密码为空，请重新输入')
        return false
      }
    }
  })

    const fn=function(){
    //传递定义的事件，将相关的值传入定义的事件
    emit('someEvent',student.email.trim(),student.password.trim())
  }

</script>

<template>
  <div>
    <table>
      <tr>
        <td><label for="">邮箱:</label></td>
        <td>
            <!-- 绑定数据 -->
          <input type="email" v-model="student.email">
        </td>
      </tr>
      <tr>
        <td><label for="">密码:</label></td>
        <td>
          <input type="password" v-model="student.password">
        </td>
      </tr>
      <tr>
        <input type="button" value="登录" @click="fn">
      </tr>
    </table>
  </div>
</template>

```

### 选项式
```vue
<script>
    export default{
        data(){
            return{
                student:{
                    name:'',
                    password:''
                }
            }
        },
        methods:{
            fn(){
                this.$emit('someEvent',this.student.name.trim(),this.student.password.trim())
            }
        },
        emits:{
            someEvent:(name,password)=>{
                if(name.trim()&& password.trim()){
                    return true
                }else{
                    if(name.trim()){
                        console.warn('输入的密码为空，请重新输入')
                        return false
                    }
                    if(password.trim()){
                        console.warn('输入的用户名为空，请重新输入')
                        return false
                    }
                console.warn('输入的用户名和密码为空，请重新输入')
                return false
                }
            }
        },
    }
</script>
```

### 父组件接收数据: App.vue
```vue
<script setup>
import HelloWorld from './components/HelloWorld.vue'

const fn=function(email,password){
  console.log('+++++++++++++')
  console.log(email)
  console.log(password)
}
</script>

<template>
  <HelloWorld @someEvent="fn" />
</template>
```
![1](./img/230421/1.png)
![2](./img/230421/2.png)
![3](./img/230421/3.png)
![4](./img/230421/4.png)