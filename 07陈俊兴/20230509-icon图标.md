```
<template>
    <div>
        <el-menu :collapse="isCollapse">
            <NavbarItem :itemMenus="menus"></NavbarItem>
        </el-menu>
    </div>
</template>
<script setup>
const props = defineProps(['menus','isCollapse'])
import NavbarItem from './NavbarItem.vue'
</script>
```
