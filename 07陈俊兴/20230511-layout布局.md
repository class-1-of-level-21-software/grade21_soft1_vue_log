```
<template>
        <!-- 遍历传入的菜单数组 -->
        <template v-for="menu in itemMenus">
            <!-- 当前菜单对象如果有下级菜单，即有children属性，并且children的长度大于0，则需要使用组件中的子菜单组件el-sub-menu -->
            <el-sub-menu v-if="menu.children && menu.children.length > 0" :index="menu.title">
                <template #title>
                    <el-icon>
                        <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                        <setting v-else></setting>
                    </el-icon>
                    {{ menu.title }}
                </template>
                <!-- 重复自己 -->
                <NavbarItem :item-menus="menu.children"></NavbarItem>
            </el-sub-menu>
            <el-menu-item v-else :index="menu.title">
                <el-icon>
                    <component v-if="menu.meta && menu.meta.icon" :is="menu.meta.icon"></component>
                    <setting v-else></setting>
                </el-icon>
                {{ menu.title }}
            </el-menu-item>
        </template>
</template>
<script setup>
const props = defineProps(['itemMenus'])

</script>
```
