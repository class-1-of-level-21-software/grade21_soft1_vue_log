~~~ html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="./vue.js"></script>
</head>
<body>
    <div id="App">
        <!-- 双大括号是VUE语法,表示告诉vue这里面放了一个变量 -->
        <h1>{{num}}{{uname}}</h1>
    </div>
    <script>
        //存放配置和数据
        const counter={
            //配置对象
            data(){
                return{
                    num:0,
                    uname:"张三"
                }
            }
        }
        Vue.createApp(counter).mount('#App')//创建对象，将配置传入，mount是进行挂载，和上面的元素相关
    </script>
</body>
</html>
~~~