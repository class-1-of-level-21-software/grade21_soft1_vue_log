# 编程式导航和components 命名视图

#
## 一.编程式导航

+ 编程式导航就是以编写代码的形式，实现 RouterLink 创建 a 标签定义导航链接的作用

#
### 导航到指定的url

+ 选项式的使用
    
    + 使用 **this.$router.push** 方法实现导航到指定的url

    + **this.$router.push** 方法会向 history 栈添加一个新的记录，当点击浏览器后退按钮时，会回到之前的 URL。

```vue
<script>
export default{
    data(){
        return{
            id:''
        }
    },
    methods:{
        fn(){
            //会跳转到 路径为/users/11的页面
            // this.$router.push('/users/11')

            //会跳转到 路径为/users/id的页面
            // this.$router.push({path:`/users/${this.id}`})

            //name:'' ：表示已命名的路由
            //params:加上参数，建立url
            this.$router.push({name:'usersId',params:{id:this.id}})
        },
    }
}
</script>
```

+ 组合式的使用

    + 导入 **useRouter** 方法，并实例化

    + 使用 **router.push** 方法实现导航到指定的url
```vue
<script setup>
import {useRouter} from 'vue-router'

const router=useRouter()

const fn=function(){
    router.push('/users/11')
}
</script>
```

+ 一些方法的使用

    + 在 **{}** ，使用 **path** 时， **params** 会被忽略

    + **params** 可以与 name (命名的路由)一起使用
```js
// 字符串路径
router.push('/users/eduardo')

// 带有路径的对象
router.push({ path: '/users/eduardo' })

// 命名的路由，并加上参数，让路由建立 url
router.push({ name: 'user', params: { username: 'eduardo' } })

// 带查询参数，结果是 /register?plan=private
router.push({ path: '/register', query: { plan: 'private' } })

// 带 hash，结果是 /about#team
router.push({ path: '/about', hash: '#team' })

```

#
### 不添加历史记录: router.replace({})

```
    1. 作用与 router.push 类似，唯一的不同是， router.push 在导航时会向 history 添加新的历史记录，而 router.replace 不会

    2. router.push 转换为 router.replace 的方法是: 添加一个 replace:true 属性
```
```js
    router.push({ path: '/home', replace: true })
    // 相当于
    router.replace({ path: '/home' })

```

#
### 横跨历史记录 :router.go

+ router.go(n) 方法采用一个整数作为参数，表示在历史堆栈中前进或后退多少步
```js
// 向前移动一条记录，与 router.forward() 相同
router.go(1)

// 返回一条记录，与 router.back() 相同
router.go(-1)

// 前进 3 条记录
router.go(3)

// 如果没有那么多记录，静默失败
router.go(-100)
router.go(100)
```

#
## components 命名视图

+ 一个视图使用一个组件渲染，对于同个路由，多个视图就需要多个组件。为了保证各组件的正确使用，在配置路由时，使用了 **components)** 配置(比原来多了个s)

#
### 单命名视图
```js
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      components: {
        default: Home,
        // UserId: UserId 的缩写
        UserId,
        // 它们与 `<RouterView>` 上的 `name` 属性匹配
        About,
      },
    },
  ],
})
```

#
### 嵌套命名路由

+ 在 children 中 使用 components 配置
```html
<div>
        这是主页
        <br>
        <Nav></Nav>
        <br>
        <div>
            <RouterView name="About"></RouterView>
            <RouterView></RouterView>
            <RouterView name="UserId"></RouterView>
        </div>

    </div>
```
```js
const routes = [
    {
        path: '/',
        component: Home,
        children: [
            {
                path:'/emails',
                component:Emails
            },{
                path:'/user',
                components:{
                    default:User,
                    //相当于 UserId :UserId
                    UserId,
                    //与 RouterView 的name 对应
                    About
                }
            }
        ]
    }, {
        path: '/:pathWatch(.*)*',
        component: NotFund
    }
]
```