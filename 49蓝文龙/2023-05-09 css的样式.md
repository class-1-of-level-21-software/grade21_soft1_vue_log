
```vue
<style scoped>
/* 侧边的样式 */
.el-aside{
  background-color: #b4c0cc;
  /* 撑开 */
  height:calc(100vh);

/* overflow:hidden作用是当元素内的内容溢出的时候使它隐藏溢出的部分，即超出部分隐藏。 */
  overflow: hidden;
}

/* 顶部样式 */
.el-header{
  background-color: rgb(101, 94, 101);
  /* 设置居中 */
  display: flex;
  align-items: center;
  /* ustify-content：项目在主轴上的对齐方式；
  space-between：两端对齐，项目之间的间隔都相等 */
   justify-content: space-between;
}

/* 主内容的样式 */
.el-main{
  background-color: gray;
}

/* 底部的样式 */
.el-footer{
background-color: rgb(44, 45, 42);

/* 弹性布局 */
display: flex;

    /* flex-direction:  决定主轴的方向（即项目的排列方向） */
    /* column：主轴为垂直方向，起点在上沿。 */
    flex-direction: column;

    /*  !important 规则用于增加样式的权重，使使用的样式具有最高优先级(优先于内联样式、内部样式)。 */
    color: white !important;
}

/* 图标的样式 */
.collapseIcon{
  /* 旋转 */
  /* 180 */
  transform:rotate(180deg)
}
.collapseIcons{
  /* 旋转90度 */
  transform:rotate(90deg)
}
</style>
```