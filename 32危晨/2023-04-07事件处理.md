# 内联事件处理器
> 内联事件处理器一般运用于简单场景，直接写在指令后面
```js
<script>
export default{
  data(){
    return {
      count:0
    }
  }     
}
</script>

<template>
<div>
  <p>计数{{ count }}</p>
</div>
<div>
  <input type="button" value="按钮" @click="count++">
</div>
</template>
```

# 方法事件处理器
>顾名思义，将你好处理的事件放在方法里面，然后再调用方法
```js
export default{
  methods:{
    but(){
      return this.count++
    }
  }
}
<div>
  <input type="button" value="按钮" @click="but">
</div>
```

# 再内联事件处理器中使用参数
### 1. 在内联事件处理器中使用**自定义**的参数
```js
export default{
  methods:{
    but(msg){
      console.log("数字"+this.count+"和消息"+msg);
      return this.count++
    }
  }
}
<div>
  <input type="button" value="按钮" @click="but('你好vue')">
</div>
```
### 2. 在内联事件处理器中使用**原生DOM事件**中定义的参数

```js
//简单修改下
methods:{
    but(msg,event){
      console.log(event);
      console.log("数字"+this.count+"和消息"+msg);
      return this.count++
    }
  }
<div>
// 箭头函数，第一种和第二种
  <input type="button" value="按钮" @click="(event)=>but('你好vue',event)">

  <input type="button" value="按钮" @click="but('你好vue',$event)"> 
</div>
```

# 冒泡
先说冒泡，我们都知道水中有气泡的时候，气泡会从水底往上升，由深往浅的。但是水在上升的过程中会经历不同的深度的水  

一个时间触发的时候，这个事件就像这个气泡一样，从dom树的底层，一层一层的往上面传递，一直传递到dom的根节点，**如果子元素和父级元素触发的是相同事件的时候，当子元素被触发的时候父元素也会被触发冒泡机制，这就是冒泡的基本原理**

```js
    methods:{
    but(msg,event){
      console.log(this.count);
      return this.count++
    },
    app(){
      console.log("app方法输出app");
    },
    newapp(){
      console.log("newapp方法输出newapp");
    }
  }

<template>
<div>
  <p>计数{{ count }}</p>
</div>
  <div @click="newapp">
    <div @click="app">
    <input type="button" value="按钮" @click="but">
  </div>
</div>
</template>
```
控制台输出
>0  
App.vue:14 app方法输出app  
App.vue:17 newapp方法输出newapp  

两个div的事件在页面上并没有人为触发，但控制台还是有输出，原因是当我们在页面点击按钮后因为冒泡的机制当子元素被触发的时候和他有相同事件的父元素也会被触发

# 事件修饰符
1. stop 单击事件将停止传递,避免冒泡操作
```javascript
<script>
    methods:{
    but(msg,event){
      console.log(this.count);
      return this.count++
    },
    app(){
      console.log("app方法输出app");
    },
    newapp(){
      console.log("newapp方法输出newapp");
    }
  }
</script>
```
```html
<template>
<div @click="newapp">
  <div @click="app">
    <input type="button" value="按钮" @click.stop="but">
  </div>
</div>
</template>
```
> 控制台输出  
App.vue:10 0  
App.vue:10 1  
App.vue:10 2

2. 汇总一下
```html
<!-- 提交事件将不再重新加载页面 -->
<form @submit.prevent="onSubmit"></form>

<!-- 修饰语可以使用链式书写 -->
<a @click.stop.prevent="doThat"></a>

<!-- 也可以只有修饰符 -->
<form @submit.prevent></form>

<!-- 仅当 event.target 是元素本身时才会触发事件处理器 -->
<!-- 例如：事件处理器不来自子元素 -->
<div @click.self="doThat">...</div>
```

# 按键修饰符
为页面上的按钮绑定一个键盘事件，当键盘上的ENTER键时会触发按钮所绑定的事件

```html
<div @click="app">
    <input type="button" value="按钮" @keyup.enter="but">
</div>
```
​
Vue 为一些常用的按键提供了别名：

.enter  
.tab  
.delete (捕获“Delete”和“Backspace”两个按键)  
.esc  
.space  
.up  
.down  
.left  
.right  



​
你可以使用以下系统按键修饰符来触发鼠标或键盘事件监听器，只有当按键被按下时才会触发。

.ctrl
.alt
.shift
.meta

1. 多个按键触发

```html
<!-- Alt + Enter -->
<input @keyup.alt.enter="clear" />

<!-- Ctrl + 点击 -->
<div @click.ctrl="doSomething">Do something</div>
```