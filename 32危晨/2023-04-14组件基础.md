父组件
```js
<script>
{/* 引入 */}
import FontSize from "./components/FontSize.vue"
export default{
  data(){
    return {
      size:1
    }
  },
  // 引入组件
  components:{
    FontSize
  },
  methods:{
    fontIncrease(){
      this.size++
      console.log(this.size);
    },
    fontDecrease(){
      let num = this.size
      if(--num == 0){
        confirm("不能再小了捏")
      }else{
        this.size--
      }
      console.log(this.size);
      
    }
  },
  mounted(){
        console.log(this.size);
        
    }
}
</script>
<template>
  <!--在另一个组件上实现对字体进行放大和减少 -->
  <div :style="{fontSize: size +'em'}">
    <FontSize msg="你好vue" @largen=fontIncrease @diminish=fontDecrease></FontSize>
  </div>

</template>

<style scoped>

</style>


```
子组件
```html
<script>
export default{
    //接收父组件中 FontSize子组件 实例化中的属性
    props:["msg"],
    emits:['largen','diminish'],
}
</script>

<template>
    <!-- 引用父组件传来的属性 -->
    <div>
        <!-- p是块标签 -->
        <p>{{ msg }}</p>
    </div>
    <div>
        <input type="button" value="+1" @click="$emit('largen')">
        <input type="button" value="-!" @click="$emit('diminish')">
    </div>
</template>
```