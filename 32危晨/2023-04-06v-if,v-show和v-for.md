# Vue项目修改默认端口的方法

## 方法一：
```
//在运行yarn dev时后面添加一个端口
yarn dev --port 端口号
yarn dev --port 10086
```

## 方法二：
```
//在配置文件package.json的包里的script中添加参数
//原来：
 "scripts": {
    "dev": "vite",          //在dev 修改
    "build": "vite build",
    "preview": "vite preview"
  },
//修改：
 "scripts": {
    "dev": "vite --port 端口号",    //"dev":"vite --port 10086"
    "build": "vite build",
    "preview": "vite preview"
  },
```

## 方法三：
```
//在配置文件vite.config.js或者是vue。config.js中添加参数

//原来
export default defineConfig({
  plugins: [vue()],
})

//添加一个server参数
export default defineConfig({
  plugins: [vue()],
  server:{
    host:'127.0.0.1',   //主机
    port:10010  //端口
  }
})
```

## 组件名渲染
* 在原来的template里随意一个标签定义一个class为"foo bar"
* 其次在申明文件里也一起添加一个class他们的元素值会一起渲染

```
//HelloWorld.vue
<template>
  <p class="foo bar">看我的标签class</p>
</template>

//App.vue
<template>
  <HelloWorld msg="Vite + Vue" class='tianjia'/>    //关键是这里后面的申明HelloWorld里多申明了class与前面的class相合并渲染
</template>

```

## 绑定内联样式
* style对象
```
<script>
  export default{
  data(){
    return{
      yColor:'blue',
      yFontSize:35
    }
  }
}
</script>

<template>
  
  <p :style="{color:yColor,fontSize:yFontSize+'px'}">内联style</p>
</template>

```

```
<script>
export default {
  data() {
    return {
      styleDDD:{
        color: 'orange',
        fontSize:'35px'
    }

  }
}
}
</script>

<template>

  <p :style="styleDDD">内联style</p>
</template>

```

* 绑定数组
```
<script>
export default {
  data() {
    return {
      colorStyles:{
        color:'green'
      },
      fontStyles:{
        fontSize:'55px'
      }
    }
  }
}
</script>

<template>

  <p :style="[colorStyles, fontStyles]">内联style</p>
</template>
```
![](https://s1.ax1x.com/2023/04/06/ppI4Ch4.png)
  


# 条件渲染

## v-if
* v-if 指令用于条件性地渲染一块内容。这块内容只会在指令的表达式返回真值时才被渲染。
* 值为true时前段代码渲染,为false时前段代码不渲染消失了

## v-else
* 就是v-if的else的相反值执行，if为TRUE执行v-if,if为FALSE执行v-else
```

<script>
export default {
  data() {
    return {
      some:false 
    }
  }
}
</script>

<template>
  <div>
    <h2 v-if="some">我是条件v-if渲染</h2>
    <h2 v-else="some">我是条件v-else渲染,v-if的值是FALSE,我两只能出现一个</h2>
  </div>

</template>
```
![](https://s1.ax1x.com/2023/04/06/ppI4oK1.png)

## v-else-if​
* v-else-if 提供的是相应于 v-if 的“else if 区块”。它可以连续多次重复使用：

## <template> 上的 v-if
* template是包装器元素在配合上v-if的FALSE值不渲染最后渲染的结果并不会包含这个 <template> 元素。
```
<div>
    <template v-if="true">
        <h1>有</h1>
        <p>还有</p>
        <span>真的有</span>
    </template>
  </div>
//修改
<div>
    <template v-if="false">
        <h1>有</h1>
        <p>还有</p>
        <span>真的有</span>
    </template>
  </div>
```
![](https://imgse.com/i/ppI5mMn)

![](https://imgse.com/i/ppI5Zxs)

## v-show
```
<h1 v-show="true">我是v-show的测试数据，你能看到我</h1>
<h2 v-show="false">我是v-show的测试数据，你不能看到我</h2>

```
* 不同之处在于 v-show 会在 DOM 渲染中保留该元素；v-show 仅切换了该元素上名为 display 的 CSS 属性。
* v-show 不支持在 <template> 元素上使用，也不能和 v-else 搭配使用。

* sp:当 v-if 和 v-for 同时存在于一个元素上的时候，v-if 会首先被执行
  
  

# 列表渲染

## v-for

* v-for 指令基于一个数组来渲染一个列表
* v-for 指令的值需要使用 item in items 形式的特殊语法，其中 items 是源数据的数组，而 item 是迭代项的别名
* v-for 也支持使用可选的第二个参数表示当前项的位置索引。

```
<script>
export default {
  data() {
    return {
      arr: [{ msg: '你爹' },
      { msg: '你娘' },
      { msg: '你兄' },
      { msg: '你弟' }
      ]
    }
  }
}
</script>

<template>
  <div>
    <ul>
      <li v-for="item in arr">
        {{ item.msg }}
      </li>
    </ul>
  </div>
</template>
```

```
<script>
export default {
  data() {
    return {
      family: '家庭',
      arr: [{ msg: '你爹' },
      { msg: '你娘' },
      { msg: '你兄' },
      { msg: '你弟' }
      ]
    }
  }
}
</script>

<template>
  <div>
    <ul>
      <li v-for="(item, index) in arr">
        {{ family }}--{{ index }}--{{ item.msg }}
      </li>
    </ul>
  </div>
</template>
```

## v-for 对象
```
export default {
  data() {
    return {
      obj: {
        bookName: '西游记',
        author: '吴承恩',
        time: '大明'
      }
    }
  }
}
<div>
    <ul>
      <li v-for="itme in obj">
        {{ itme }}
      </li>
    </ul>
</div>
```

* itme是对象函数里的值，key是属性名,index是对象里的下标
```
<li v-for="(itme,key,index) in obj">
        {{index}}--{{ key +':'}}--{{ itme }}
</li>
```

## key 管理状态

* Vue 默认按照“就地更新”的策略来更新通过 v-for 渲染的元素列表。当数据项的顺序改变时，Vue 不会随之移动 DOM 元素的顺序，而是就地更新每个元素，确保它们在原本指定的索引位置上渲染。

* 给一个元素的标签里加一个key的id 在后期的修改数据的庞大时单个处理延迟很长有了id就很快处理

## 变更方法
* Vue 能够侦听响应式数组的变更方法，并在它们被调用时触发相关的更新。这些变更方法包括：
```
push()
pop()
shift()
unshift()
splice()
sort()
reverse()
```

```
<script>
export default {
  data() {
    return {
      numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    }
  },
  computed: {
    arr() {
      
       return this.numbers.filter(itme => itme % 2 === 0)
      
    }
  }
}
</script>

<template>
  <div>
    <ul>
      <li v-for="itme in arr">
        {{ itme }}
    </ul>
  </div>
</template>
```
```
numbers: [1,3,5,7,9,2,4,6,8]

computed: {
    arr() {
      
       return this.numbers.sort()
      
    }
  }
  <li v-for="itme in arr">
        {{ itme }}
      </li>

```
