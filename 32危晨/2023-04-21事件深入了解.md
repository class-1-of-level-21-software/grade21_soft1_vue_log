# 父组件与子组件
父组件和子组件的定义是相对而言的，引用方是子组件，被引用方是父组件

事件在子组件上被抛出，再有父组件来监听

# emit
1. 子组件抛出事件，要在emits对象中显示声明抛出的事件

```js
    emits:['fatherBtn','sysOut'],
```

2. 子组件抛出事件的方式
父组件
```js
<script>
//导入组件
import Tmp1Components from './components/Tmp1Components.vue';
import Tmp2Components from './components/Tmp2Components.vue';
import Tmp3Components from './components/Tmp3Components.vue';
export default{
  data(){
    return {
      count:1
    }
  },
  // 定义组件
  components:{
    Tmp1Components,
    Tmp2Components,
    Tmp3Components,
  },
  methods:{
    addCount(){
      
      console.log("你好Vue");
    },
    hl(){
      this.count++
      console.log(this.count);
    }
  }
}
</script>

<template>
<Tmp1Components @fatherBtn="addCount" @sysOut="hl"/>
</template>

<style scoped>

</style>
```
子组件
```html
    <script>
export default {
    emits:['fatherBtn','sysOut'],
    methods:{
        slo(){
            this.$emit('sysOut')
        }
    }

}
</script>

<template>
<div>
    <!-- 方式一 -->
    <input type="button" value="点我" @click="$emit('fatherBtn')">
</div>
<div>
    <!-- 方式二 -->
    <input type="button" value="点我1" @click="slo">
</div>
</template>
```
3. 参数参与运算
父组件
```js
 methods:{
    count(x,y,z){
      let all = x+y+z
      console.log(all);
    }
  }
  <Tmp2Components @numberFn="count"/>
```
子组件
```js
<script>
export default{
    emits:['numberFn']
}
</script>

<template>
   <div>
    <input type="button" value="点我输出数字" @click="$emit('numberFn',1,2,3)">
   </div>
</template>
```
# 事件校验
在子组件中进行校验，根据校验结果判断是否抛出警告
```HTML
<script>
export default {
    data(){
        return {
            email:'',
            pwd:''
        }
    },
    emits:{
        tableFn:({email,pwd})=>{
            if(email && pwd){
                return true
            }else{
                console.log("失败了捏");
                return false
            }
            
        }
    },
}
</script>

<template>
<div>
    <table>
        <tr>
            <td><label>邮箱地址：</label></td>
            <td>
                <input type="text" placeholder="请输入邮箱地址" v-model.trim="email">
            </td>
        </tr>
        <tr>
            <td><label>密码：</label></td>
            <td>
                <input type="password" name="" id="" v-model.trim="pwd">
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="提交" @click="$emit('tableFn',email,pwd)">
            </td>
            <td></td>
        </tr>
    </table>
</div>
</template>
```
父组件
<script>
//导入组件
import Tmp1Components from './components/Tmp1Components.vue';
import Tmp2Components from './components/Tmp2Components.vue';
import Tmp3Components from './components/Tmp3Components.vue';
export default{
  data(){
    return {
      
    }
  },
  // 定义组件
  components:{
    Tmp1Components,
    Tmp2Components,
    Tmp3Components,
  },
  methods:{
    count(x,y,z){
      let all = x+y+z
      console.log(all);
    },
    talbeMessage(email,pwd){
      console.log(email);
      console.log(pwd);
    }
  }

}
</script>

<template>
<Tmp1Components @tableFn="talbeMessage"/>
<Tmp2Components @numberFn="count"/>
</template>

<style scoped>

</style>

```
