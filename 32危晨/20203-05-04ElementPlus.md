# ElemenPlus中Menu模板
```html
<!-- 菜单 -->
<el-menu></el-menu>
<!-- 子菜单 -->
<el-sub-menu></el-sub-menu>
<!-- 菜单栏 -->
<el-sub-item></el-sub-item>
```
## 示例
```html
<template>
    <el-menu>
        <el-sub-menu index="1">
            <!-- 编辑子菜单的名称 -->
            <template #title>
                <span>学生菜单</span>
            </template>
            <!-- 内容 -->
            <el-menu-item index="1-1">item One</el-menu-item>
            <el-menu-item index="1-2">item Two</el-menu-item>
        </el-sub-menu>
        <el-sub-menu index="2">
            <template #title>
                <span>教师菜单</span>
            </template>
            <el-menu-item index="2-1">元素一</el-menu-item>
            <!-- 菜单中嵌套菜单 -->
            <el-sub-menu index="2-2">
                <template #title>
                    <span>教师等级</span>
                </template>
                <el-menu-item index="2-2-1">初级教师</el-menu-item>
                <el-menu-item index="2-2-2">中级教师</el-menu-item>
            </el-sub-menu>
        </el-sub-menu>
        <el-sub-menu  index="3">
            <template #title>
                <span>年级菜单</span>
            </template>
            <el-menu-item index="3-1">初一</el-menu-item>
        </el-sub-menu>
    </el-menu>
</template>
```